
.globl main
.data
prompt: .asciiz "\nSelect a program: (1) median   (2) mean  (3) division " #offset = 0
presult0: .asciiz "\nmedian: Enter a number" #offset = 58
presult1: .asciiz "\nmean: Enter a number" #offset = 80
presult2: .asciiz "\ndiv: Enter number for p: " #offset = 99
presult2A: .asciiz "\ndiv: Enter number for q: " #offset = 125
result0: .asciiz "\nthe median is " #offset =141
result1: .asciiz "\nthe mean is " #offset = 169
result2: .asciiz "\nthe quotient is " #offset = 187
result2A: .asciiz "\nthe remainder is " #offset = 204
continue:	.asciiz "\nEnter another number?(1 = yes, 2 = no) " #offset = 224
#array1:	.space 200	#the array for users to put 50 numbers
#array2: .space 200	#the array for users to put 50 numbers
.text
main:
	 add $s4, $ra, $0     # Save ra into s4
	 lui $a0, 0x1000		#print prompt			
	 addi $v0, $0, 4
	 syscall

	 addi $v0, $0, 5		#get int input from user
	 syscall
	 add $t0, $v0, $0
	 addi $s1, $0, 1		# correct input of choices in the program
	 addi $s2, $0, 2
	 addi $s3, $0, 3
	 add $t1, $0, $s1	   
	 add $t2, $0, $s2
	 add $t3, $0, $s3
	 add $a0, $t0, $0     # Pass in user entered number
         add $t0,$0,$0
	 beq $a0, $t1, median 
	 beq $a0, $t2, mean
	 beq $a0, $t3, division
	 nor $0, $0, $0       # Branch delay slot
	
median:
	 lui $a0, 0x1000  
	 addi $a0, $a0, 55
	 addi $v0, $0, 4
	 syscall


mean: 
	
     lui $a0, 0x1000 #load mean intruct.
     addi $a0, $a0, 80
     addi $v0, $0, 4
     syscall
     addi $v0, $0, 5 #get input from user for p divisor
     syscall
     add $t0, $v0, $t0 #user input is saved
	 
	 
     addi $s5, $s5, 1 #increment q the dividend

     lui $a0, 0x1000 #load mean instruct.2
     addi $a0, $a0, 224
     addi $v0, $0, 4
     syscall
  
     addi $v0, $0, 5
     syscall

    # beq $v0,2,calc
    # nor $0,$0,$0

     add $v1, $0, $0
     addi $t6,$0,1
     addi $t7,$0,2
     beq $v0, $t6, mean
     nor $0,$0,$0  
     
	 beq $v0, $t7, calc
	 nor $0, $0, $0
	 
calc:
	 sub $v0, $t0, $s5  
     bltz $v0, finm
     addi $v1, $v1, 1
     sub $t0, $t0, $s5
     slt $t3, $t0, $s5  
     bne $t3, $0, finm
     nor $0,$0,$0
     j calc
     nor $0, $0, $0
	 
	 
finm:    
	 lui $a0, 0x1000	#return the quotient to the user
	 addi $a0, $a0, 187
	 addi $v0, $0, 4	#get the answer for user
	 syscall
	 
	 add $a0, $0, $v1	#show quotient
	 addi $v0, $0, 1	#again, display quotient to the user
     syscall
	 nor $0, $0, $0
	
	 lui $a0, 0x1000	#return the remainder to the user
	 addi $a0, $a0, 204
	 addi $v0, $0, 4    #get the remainder
	 syscall
	 nor $0, $0, $0
	 
	 add $a0, $0, $s0   #show remainder
	 addi $v0, $0, 1    #then, display the remainder to the user
	 syscall
	 nor $0, $0, $0
	 
	 lui $a0, 0x1000	#return mean
	 addi $a0, $a0, 172
	 addi $v0, $0, 4	#get anser for user
	 syscall
	 
	 add $a0, $0, $v1	#show mean to user
	 addi $v0, $0, 1	#display the mean
	 syscall
	 nor $0, $0, $0
	 
	 add $ra, $s4, $0
	 jal main
     nor $0, $0, $0     #Branch delay slot
	 
division:	 
	 lui $a0, 0x1000	
	 add $t0, $0, $0
	 addi $a0, $a0, 103	#load presult2
	 addi $v0, $0, 4
	 syscall
	
	 addi $v0, $0, 5	#get input from user for p divisor
	 syscall
	 add $t0, $v0, $0	#user input is saved
	 add $s0, $t0, $0   #Pass in user entered number
	 
	 lui $a0, 0x1000	
	 addi $a0, $a0, 130	#load presult2A
	 addi $v0, $0, 4	
	 syscall
	 
	 addi $v0, $0, 5	#get input from user for q dividend
	 syscall
	 add $t0, $v0, $0	#user input saved
	 add $s1, $t0, $0   #Pass in user entered number	
	                    
						#the division starts here
	 add $v1, $0, $0    #$v1 is the quotient
	 nor $0, $0, $0	    
	 
again:
     sub $v0, $s0, $s1 	
	 bltz $v0, finish
	 addi $v1, $v1, 1
	 sub $s0, $s0, $s1
	 slt $t3, $s0, $s1  
	 bne $t3, $0, finish
	 jal again
	 nor $0, $0, $0
	 
finish:
	 lui $a0, 0x1000	#return the quotient to the user
	 addi $a0, $a0, 187
	 addi $v0, $0, 4	#get the answer for user
	 syscall
	 
	 add $a0, $0, $v1	#show quotient
	 addi $v0, $0, 1	#again, display quotient to the user
     syscall
	 nor $0, $0, $0
	
	 lui $a0, 0x1000	#return the remainder to the user
	 addi $a0, $a0, 204
	 addi $v0, $0, 4    #get the remainder
	 syscall
	 nor $0, $0, $0
	
	 add $a0, $0, $s0 #show remainder
	 addi $v0, $0, 1  #then, display the remainder to the user
	 syscall
	 nor $0, $0, $0
     
	 nor $0, $0, $0
	 add $ra, $s4, $0
	 jal main
     nor $0, $0, $0     #Branch delay slot
	 

	 
