#program: hw10.s
#This code calculates factorial 0-10
	.data
	.align 2
	.space 12
String:	.space 16
Input:	.asciiz "\nEnter an integer between 1-10\n"
Output:	.asciiz "\n\n\nThe Factorial of number entered is "

	.text
	.globl main
main:
	li $2,4
	la $4,Input
	syscall

	li $2,5
	syscall
	move $16,$2

	move $4,$2
	jal Check
	nop

	addiu $17,$0,1
	move $15,$16
while:	beqz $15,Answer
	nop

	mul $17,$17,$15
	addi $15,$15,-1
	b while

Answer:
	li $2,4
	la $4,Output
	syscall

	li $2,1
	move $4,$17
	syscall

	b Exit

	.text
	.align 2
Check:
	move $8,$4
	bltz $8,Exit
	nop
	bgt $8,10,Exit
	nop
	jr $31
Exit:
	li $2,10
	syscall