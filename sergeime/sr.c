#include <stdio.h>

/* ******************************************************************
 ALTERNATING BIT AND GO-BACK-N NETWORK EMULATOR: VERSION 1.1  J.F Kurose

   This code should be used for PA2, unidirectional or bidirectional
   data transfer protocols (from A to B. Bidirectional transfer of data
   is for extra credit and is not required).  Network properties:
   - one way network delay averages five time units (longer if there
     are other messages in the channel for GBN), but can be larger
   - packets can be corrupted (either the header or the data portion)
     or lost, according to user-defined probabilities
   - packets will be delivered in the order in which they were sent
     (although some can be lost).
**********************************************************************/

#define BIDIRECTIONAL 0    /* change to 1 if you're doing extra credit */
                           /* and write a routine called B_output */

/* a "msg" is the data unit passed from layer 5 (teachers code) to layer  */
/* 4 (students' code).  It contains the data (characters) to be delivered */
/* to layer 5 via the students transport level protocol entities.         */
struct msg {
  char data[20];
  };

/* a packet is the data unit passed from layer 4 (students code) to layer */
/* 3 (teachers code).  Note the pre-defined packet structure, which all   */
/* students must follow. */
struct pkt {
   int seqnum;
   int acknum;
   int checksum;
   char payload[20];
    };

/********* STUDENTS WRITE THE NEXT SEVEN ROUTINES *********/
struct pkt packet_array[2500],packet_array_B[2500];
struct pkt packet_temp;
int num = 0;
int index = 0;
int counter_send=0;
int counter_recv=0;

int receive_seqnum;
int base;
int packet_seqnum = 1;
int ack_num=1;
int number = 0;
int buf_num=1;
struct pkt packet_og;
struct pkt packet_previous;
struct pkt packet_hist;
int flag_variable = 0;
int acknowledgement_array[2500];
int counter_analysis_1 =0;
int counter_analysis_2 =0;
int counter_analysis_3 =0;
int counter_analysis_4 =0;
int size_window = 500;
int head = 0;
int flag_buffer = 0;
int flag_another = 0;
float timer_val = 31;
int next_expected_seqnum;
int counter_ack = 0;



/* called from layer 5, passed the data to be sent to other side */
A_output(message)
  struct msg message;
{

	int i;
	int sum = 0;
	for(i=0;i<20;i++)
	{
	packet_og.payload[i]=message.data[i];
	sum = sum + packet_og.payload[i];
	packet_array[num].payload[i] = packet_og.payload[i];
	}

counter_analysis_1++;

packet_array[num].seqnum = packet_seqnum;

packet_array[num].acknum = packet_array[num].seqnum;

packet_array[num].checksum = sum + packet_array[num].seqnum + packet_array[num].acknum;

printf("\n the sequence number for this packet is : %d",packet_array[num].seqnum);
printf("the checksum for this packet is : %d\n",packet_array[num].checksum);
printf("\n the payload in the array is %s",packet_array[num].payload);

if(packet_array[num].seqnum<(base + size_window))
    {
	
    printf("\n the packet is sent");
	
	/*if(head <(base + size_window))
	    {
	    while(buf_num<=packet_seqnum)
	    {
	    tolayer3(0,packet_array[num]);
	    head = num;
    	    head++;
	    buf_num++;
    	    }
	    }*/
 
	tolayer3(0,packet_array[num]);
  	counter_analysis_2++;   
	if(base==packet_seqnum)
        {
        printf("timer about to be started ");
        starttimer(0,timer_val);
        }     	  
	    counter_analysis_2++;   
  	    printf("seq no is %d\n",packet_array[num].seqnum);
            num++;   
	    packet_seqnum++;
	}
}

B_output(message)  /* need be completed only for extra credit */
  struct msg message;
{

}

/* called from layer 3, when a packet arrives for layer 4 */
A_input(packet)
  struct pkt packet;
{
printf("reached the A_input packet function \n");
//printf("checksum received is %d",packet.checksum);
printf("the sequence number of the packet received is %d \n",packet.seqnum);
printf("the acknowledgement number is %d \n",packet.acknum);
printf("the data received is :%s\n",packet.payload);

   int i;
   int sum=0;
   
    for(i=0;i<20;i++)
    {
    sum = sum + packet.payload[i];
    }       
    sum = sum + packet.seqnum + packet.acknum;
 
	//printf("the value of the sum is %d",sum);

	printf("the base is %d \n",base);
	printf("the packet_seqnum is %d \n",packet_seqnum); 
 

 if(base==packet.acknum)
{    	   		
	base = packet.acknum + 1;	
	int m; 
	for(m=0;m<=counter_ack;m++)
	{
		if(base==acknowledgement_array[m])	
		{	
		    base++;
		    m=0;
		    printf("\n base is %d",base);
        	}      
	}
       
	//printf("\n the base value after sliding it ahead");	
	 if(base==packet_seqnum)
 	       {
 	         stoptimer(0);	
    	   	 printf("\n reached this phase");
    	       }
   	       else
   	       {
  	         //stoptimer(0);
  	           starttimer(0,timer_val);
 	       }		
}
	else if(packet.acknum > base && packet.acknum < packet_seqnum)
	{
		int k;
		//mark the packet inside the window as acknowledged 
		//nothing to be done to the timer
		printf("\n reached that part where i don't know what to do further bhenchod \n");
		for(k=0;k<=counter_ack;k++)
		{		
			if(packet.acknum==acknowledgement_array[k])
			{		
			flag_another = 1;	
			printf("the acknowlegement number for this packet is received");			
			}
			break;		
			}
		if(flag_another==0)
		     {
		    	printf("reached inside the for loop of the acknowledgement and inserting in the array\n");
		    	printf("counter_ack is : %d \n",counter_ack); 
		    	acknowledgement_array[counter_ack] = packet.acknum;
		    	counter_ack++;
		     }
	}
}

/* called when A's timer goes off */
A_timerinterrupt()
{
printf("this function is called because the timer timed out with base %d \n",base);
starttimer(0,timer_val); //only for the base 
tolayer3(0,packet_array[base-1]);//sending the base packet and no other packet is sent they may have been sent already
counter_analysis_2++;
printf("sending the base: %d \n",base);
}  

/* the following routine will be called once (only) before any other */
/* entity A routines are called. You can use it to do any initialization */
A_init()
{
base = 1;
packet_array[0].seqnum = packet_array[0].acknum = 1;

}


/* Note that with simplex transfer from a-to-B, there is no B_output() */

/* called from layer 3, when a packet arrives for layer 4 at B*/
B_input(packet)
  struct pkt packet;
{
 int i;
         int sum = 0;
         counter_analysis_3++;  
 	for(i=0;i<20;i++)
	 {	
  	  sum = sum + packet.payload[i];      
   	 }    
    	  sum = sum + packet.seqnum + packet.acknum;
   
    	printf("\n the sequence num and the acknowlegement number is calculated to be %d and %d \n",packet.seqnum,packet.acknum);
    	printf("\n inside the B_INPUT function and the checksum is calculated to be %d \n",sum);
    	printf("the data received is : %s \n",packet.payload);   
    	printf("\n the packet.checksum is calculated to be %d \n",packet.checksum);  

	if(sum==packet.checksum && receive_seqnum==packet.seqnum)
	    { 
        	tolayer5(1,packet.payload);
		counter_analysis_4++;	        
		printf("the data trying to deliver to layer 5 is %s \n",packet.payload);
		packet.acknum=receive_seqnum;
        	packet.checksum = sum;
		tolayer3(1,packet); /*sending the acknowledgement*/
        	ack_num++;       
        	receive_seqnum++;
	    	printf("after incrementing the numbers %d and %d",index,receive_seqnum);
		int am;
			for(am=0;am<index;am++)
			{
				if(packet_array_B[am].seqnum==receive_seqnum)
				{
				// this part for the next expected sequence number
				tolayer5(1,packet_array_B[index].payload);
				counter_analysis_4++;
				printf("\n delivering the next buffered packet of data tolayer5 \n");
				tolayer3(1,packet_array_B[index]); /*sending acknowledgement of the packet which is next in the buffer*/
				index++;
				receive_seqnum++;
				printf("after delivering the buffered packet %d \n",index);
				}	
			}
	   }
	else if(sum==packet.checksum && receive_seqnum < packet.seqnum && packet.seqnum < (receive_seqnum + size_window))
	{
	int i;
	printf("\n the received sequence number is less than the sequence number of the packet");
	for(i=0;i<=index;i++)
		{
			if(packet_array_B[i].seqnum == packet.seqnum) // packet is already buffered acknowledging the packet and dropping the duplicate
			{
			printf("\n packet already buffered so just sending the acknowledgement \n");
			tolayer3(1,packet);
			flag_buffer = 1;
			printf("setting the buffer flag \n");
			}
		}	
		if(flag_buffer==0)
		{
		printf("\n reached where the buffer flag is 0 \n");
		printf("adding to the buffer \n");
		printf("the value of index is %d",index);		
		packet_array_B[index].seqnum = packet.seqnum;
		packet_array_B[index].acknum = packet.acknum;	

		int sum_arr = 0;
		int j;
		for(j=0;j<20;j++)
		{	
  		  sum_arr = sum_arr + packet.payload[j];
  		}        		
		packet_array_B[index].checksum = sum_arr + packet_array_B[index].seqnum + packet_array_B[index].acknum;
		tolayer3(1,packet); // sending the acknowledgement because the flag is set 
		printf("the acknowledgement is sent and index is incremented \n");
		index++;
		}		
        }
	else if(packet.seqnum>=(receive_seqnum - size_window) && packet.seqnum < receive_seqnum)
	{
	printf("the last loop inside the B-input function..\n");
	tolayer3(1,packet);
	//index++;
	}
flag_buffer = 0;
}

/* called when B's timer goes off */
B_timerinterrupt()
{
}

/* the following rouytine will be called once (only) before any other */
/* entity B routines are called. You can use it to do any initialization */
B_init()
{
next_expected_seqnum = 1;
receive_seqnum = 1;
printf("the expected sequence number is %d",receive_seqnum);
}


/*****************************************************************
***************** NETWORK EMULATION CODE STARTS BELOW ***********
The code below emulates the layer 3 and below network environment:
  - emulates the tranmission and delivery (possibly with bit-level corruption
    and packet loss) of packets across the layer 3/4 interface
  - handles the starting/stopping of a timer, and generates timer
    interrupts (resulting in calling students timer handler).
  - generates message to be sent (passed from later 5 to 4)

THERE IS NOT REASON THAT ANY STUDENT SHOULD HAVE TO READ OR UNDERSTAND
THE CODE BELOW.  YOU SHOLD NOT TOUCH, OR REFERENCE (in your code) ANY
OF THE DATA STRUCTURES BELOW.  If you're interested in how I designed
the emulator, you're welcome to look at the code - but again, you should have
to, and you defeinitely should not have to modify
******************************************************************/

struct event {
   float evtime;           /* event time */
   int evtype;             /* event type code */
   int eventity;           /* entity where event occurs */
   struct pkt *pktptr;     /* ptr to packet (if any) assoc w/ this event */
   struct event *prev;
   struct event *next;
 };
struct event *evlist = NULL;   /* the event list */

/* possible events: */
#define  TIMER_INTERRUPT 0  
#define  FROM_LAYER5     1
#define  FROM_LAYER3     2

#define  OFF             0
#define  ON              1
#define   A    0
#define   B    1



int TRACE = 1;             /* for my debugging */
int nsim = 0;              /* number of messages from 5 to 4 so far */ 
int nsimmax = 0;           /* number of msgs to generate, then stop */
float time = 0.000;
float lossprob;            /* probability that a packet is dropped  */
float corruptprob;         /* probability that one bit is packet is flipped */
float lambda;              /* arrival rate of messages from layer 5 */   
int   ntolayer3;           /* number sent into layer 3 */
int   nlost;               /* number lost in media */
int ncorrupt;              /* number corrupted by media*/

main()
{
   struct event *eventptr;
   struct msg  msg2give;
   struct pkt  pkt2give;
   
   int i,j;
   char c; 
  
   init();
   A_init();
   B_init();
   
   while (1) {
        eventptr = evlist;            /* get next event to simulate */
        if (eventptr==NULL)
           goto terminate;
        evlist = evlist->next;        /* remove this event from event list */
        if (evlist!=NULL)
           evlist->prev=NULL;
        if (TRACE>=2) {
           printf("\nEVENT time: %f,",eventptr->evtime);
           printf("  type: %d",eventptr->evtype);
           if (eventptr->evtype==0)
	       printf(", timerinterrupt  ");
             else if (eventptr->evtype==1)
               printf(", fromlayer5 ");
             else
	     printf(", fromlayer3 ");
           printf(" entity: %d\n",eventptr->eventity);
           }
        time = eventptr->evtime;        /* update time to next event time */
        if (nsim==nsimmax)
	  break;                        /* all done with simulation */
        if (eventptr->evtype == FROM_LAYER5 ) {
            generate_next_arrival();   /* set up future arrival */
            /* fill in msg to give with string of same letter */    
            j = nsim % 26; 
            for (i=0; i<20; i++)  
               msg2give.data[i] = 97 + j;
            if (TRACE>2) {
               printf("          MAINLOOP: data given to student: ");
                 for (i=0; i<20; i++) 
                  printf("%c", msg2give.data[i]);
               printf("\n");
	     }
            nsim++;
            if (eventptr->eventity == A) 
               A_output(msg2give);  
             else
               B_output(msg2give);  
            }
          else if (eventptr->evtype ==  FROM_LAYER3) {
            pkt2give.seqnum = eventptr->pktptr->seqnum;
            pkt2give.acknum = eventptr->pktptr->acknum;
            pkt2give.checksum = eventptr->pktptr->checksum;
            for (i=0; i<20; i++)  
                pkt2give.payload[i] = eventptr->pktptr->payload[i];
	    if (eventptr->eventity ==A)      /* deliver packet by calling */
   	       A_input(pkt2give);            /* appropriate entity */
            else
   	       B_input(pkt2give);
	    free(eventptr->pktptr);          /* free the memory for packet */
            }
          else if (eventptr->evtype ==  TIMER_INTERRUPT) {
            if (eventptr->eventity == A) 
	       A_timerinterrupt();
             else
	       B_timerinterrupt();
             }
          else  {
	     printf("INTERNAL PANIC: unknown event type \n");
             }
        free(eventptr);
        }

terminate:
   printf(" Simulator terminated at time %f\n after sending %d msgs from layer5\n",time,nsim);
   printf("Protocol:Selective Repeat");
   printf("\n%d of packets sent from the Application Layer of Sender A \n",counter_analysis_1);
   printf("%d of packets sent from the Transport Layer of Sender A\n",counter_analysis_2);
   printf("%d packets recieved at the Transport layer of Receiver B \n",counter_analysis_3);
   printf("%d of packets received at Application layer of Receiver B \n",counter_analysis_4);
   printf("Total time:%d time units",time);
   float through;
   through = counter_analysis_4/time;
   printf("\nthe Throughput is   :  %f\n",through);

}



init()                         /* initialize the simulator */
{
  int i;
  float sum, avg;
  float jimsrand();
  
  
   printf("-----  Stop and Wait Network Simulator Version 1.1 -------- \n\n");
   printf("Enter the number of messages to simulate: ");
   scanf("%d",&nsimmax);
   printf("Enter  packet loss probability [enter 0.0 for no loss]:");
   scanf("%f",&lossprob);
   printf("Enter packet corruption probability [0.0 for no corruption]:");
   scanf("%f",&corruptprob);
   printf("Enter average time between messages from sender's layer5 [ > 0.0]:");
   scanf("%f",&lambda);
   printf("Enter TRACE:");
   scanf("%d",&TRACE);

   srand(9999);              /* init random number generator */
   sum = 0.0;                /* test random number generator for students */
   for (i=0; i<1000; i++)
      sum=sum+jimsrand();    /* jimsrand() should be uniform in [0,1] */
   avg = sum/1000.0;
   if (avg < 0.25 || avg > 0.75) {
    printf("It is likely that random number generation on your machine\n" ); 
    printf("is different from what this emulator expects.  Please take\n");
    printf("a look at the routine jimsrand() in the emulator code. Sorry. \n");
    exit(1);
    }

   ntolayer3 = 0;
   nlost = 0;
   ncorrupt = 0;

   time=0.0;                    /* initialize time to 0.0 */
   generate_next_arrival();     /* initialize event list */
}

/****************************************************************************/
/* jimsrand(): return a float in range [0,1].  The routine below is used to */
/* isolate all random number generation in one location.  We assume that the*/
/* system-supplied rand() function return an int in therange [0,mmm]        */
/****************************************************************************/
float jimsrand() 
{
  double mmm = 2147483647;   /* largest int  - MACHINE DEPENDENT!!!!!!!!   */
  float x;                   /* individual students may need to change mmm */ 
  x = rand()/mmm;            /* x should be uniform in [0,1] */
  return(x);
}  

/********************* EVENT HANDLINE ROUTINES *******/
/*  The next set of routines handle the event list   */
/*****************************************************/
 
generate_next_arrival()
{
   double x,log(),ceil();
   struct event *evptr;
    char *malloc();
   float ttime;
   int tempint;

   if (TRACE>2)
       printf("          GENERATE NEXT ARRIVAL: creating new arrival\n");
 
   x = lambda*jimsrand()*2;  /* x is uniform on [0,2*lambda] */
                             /* having mean of lambda        */
   evptr = (struct event *)malloc(sizeof(struct event));
   evptr->evtime =  time + x;
   evptr->evtype =  FROM_LAYER5;
   if (BIDIRECTIONAL && (jimsrand()>0.5) )
      evptr->eventity = B;
    else
      evptr->eventity = A;
   insertevent(evptr);
} 


insertevent(p)
   struct event *p;
{
   struct event *q,*qold;

   if (TRACE>2) {
      printf("            INSERTEVENT: time is %lf\n",time);
      printf("            INSERTEVENT: future time will be %lf\n",p->evtime); 
      }
   q = evlist;     /* q points to header of list in which p struct inserted */
   if (q==NULL) {   /* list is empty */
        evlist=p;
        p->next=NULL;
        p->prev=NULL;
        }
     else {
        for (qold = q; q !=NULL && p->evtime > q->evtime; q=q->next)
              qold=q; 
        if (q==NULL) {   /* end of list */
             qold->next = p;
             p->prev = qold;
             p->next = NULL;
             }
           else if (q==evlist) { /* front of list */
             p->next=evlist;
             p->prev=NULL;
             p->next->prev=p;
             evlist = p;
             }
           else {     /* middle of list */
             p->next=q;
             p->prev=q->prev;
             q->prev->next=p;
             q->prev=p;
             }
         }
}

printevlist()
{
  struct event *q;
  int i;
  printf("--------------\nEvent List Follows:\n");
  for(q = evlist; q!=NULL; q=q->next) {
    printf("Event time: %f, type: %d entity: %d\n",q->evtime,q->evtype,q->eventity);
    }
  printf("--------------\n");
}



/********************** Student-callable ROUTINES ***********************/

/* called by students routine to cancel a previously-started timer */
stoptimer(AorB)
int AorB;  /* A or B is trying to stop timer */
{
 struct event *q,*qold;

 if (TRACE>2)
    printf("          STOP TIMER: stopping timer at %f\n",time);
/* for (q=evlist; q!=NULL && q->next!=NULL; q = q->next)  */
 for (q=evlist; q!=NULL ; q = q->next) 
    if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) { 
       /* remove this event */
       if (q->next==NULL && q->prev==NULL)
             evlist=NULL;         /* remove first and only event on list */
          else if (q->next==NULL) /* end of list - there is one in front */
             q->prev->next = NULL;
          else if (q==evlist) { /* front of list - there must be event after */
             q->next->prev=NULL;
             evlist = q->next;
             }
           else {     /* middle of list */
             q->next->prev = q->prev;
             q->prev->next =  q->next;
             }
       free(q);
       return;
     }
  printf("Warning: unable to cancel your timer. It wasn't running.\n");
}


starttimer(AorB,increment)
int AorB;  /* A or B is trying to stop timer */
float increment;
{

 struct event *q;
 struct event *evptr;
 char *malloc();

 if (TRACE>2)
    printf("          START TIMER: starting timer at %f\n",time);
 /* be nice: check to see if timer is already started, if so, then  warn */
/* for (q=evlist; q!=NULL && q->next!=NULL; q = q->next)  */
   for (q=evlist; q!=NULL ; q = q->next)  
    if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) { 
      printf("Warning: attempt to start a timer that is already started\n");
      return;
      }
 
/* create future event for when timer goes off */
   evptr = (struct event *)malloc(sizeof(struct event));
   evptr->evtime =  time + increment;
   evptr->evtype =  TIMER_INTERRUPT;
   evptr->eventity = AorB;
   insertevent(evptr);
} 


/************************** TOLAYER3 ***************/
tolayer3(AorB,packet)
int AorB;  /* A or B is trying to stop timer */
struct pkt packet;
{
 struct pkt *mypktptr;
 struct event *evptr,*q;
 char *malloc();
 float lastime, x, jimsrand();
 int i;


 ntolayer3++;

 /* simulate losses: */
 if (jimsrand() < lossprob)  {
      nlost++;
      if (TRACE>0)    
	printf("          TOLAYER3: packet being lost\n");
      return;
    }  

/* make a copy of the packet student just gave me since he/she may decide */
/* to do something with the packet after we return back to him/her */ 
 mypktptr = (struct pkt *)malloc(sizeof(struct pkt));
 mypktptr->seqnum = packet.seqnum;
 mypktptr->acknum = packet.acknum;
 mypktptr->checksum = packet.checksum;
 for (i=0; i<20; i++)
    mypktptr->payload[i] = packet.payload[i];
 if (TRACE>2)  {
   printf("          TOLAYER3: seq: %d, ack %d, check: %d ", mypktptr->seqnum,
	  mypktptr->acknum,  mypktptr->checksum);
    for (i=0; i<20; i++)
        printf("%c",mypktptr->payload[i]);
    printf("\n");
   }

/* create future event for arrival of packet at the other side */
  evptr = (struct event *)malloc(sizeof(struct event));
  evptr->evtype =  FROM_LAYER3;   /* packet will pop out from layer3 */
  evptr->eventity = (AorB+1) % 2; /* event occurs at other entity */
  evptr->pktptr = mypktptr;       /* save ptr to my copy of packet */
/* finally, compute the arrival time of packet at the other end.
   medium can not reorder, so make sure packet arrives between 1 and 10
   time units after the latest arrival time of packets
   currently in the medium on their way to the destination */
 lastime = time;
/* for (q=evlist; q!=NULL && q->next!=NULL; q = q->next) */
 for (q=evlist; q!=NULL ; q = q->next) 
    if ( (q->evtype==FROM_LAYER3  && q->eventity==evptr->eventity) ) 
      lastime = q->evtime;
 evptr->evtime =  lastime + 1 + 9*jimsrand();
 


 /* simulate corruption: */
 if (jimsrand() < corruptprob)  {
    ncorrupt++;
    if ( (x = jimsrand()) < .75)
       mypktptr->payload[0]='Z';   /* corrupt payload */
      else if (x < .875)
       mypktptr->seqnum = 999999;
      else
       mypktptr->acknum = 999999;
    if (TRACE>0)    
	printf("          TOLAYER3: packet being corrupted\n");
    }  

  if (TRACE>2)  
     printf("          TOLAYER3: scheduling arrival on other side\n");
  insertevent(evptr);
} 

tolayer5(AorB,datasent)
  int AorB;
  char datasent[20];
{
  int i;  
  if (TRACE>2) {
     printf("          TOLAYER5: data received: ");
     for (i=0; i<20; i++)  
        printf("%c",datasent[i]);
     printf("\n");
   }
  
}
