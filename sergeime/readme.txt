50030322-Sergei Mellow-sergeime


ReadMe
1) to make all the programs just use the Makefile. It gives warnings but still works.

2) All the important variables are declared at the start of all programs. 
This includes all my analysis variables.

3) To run simply type './gbn' or  './sr' or  './abt' depending on which you need to run.

4) I left the window sizes at 500 for SR and GBN.

5) I was playing around with the increament values on the timers.
I left them on  sr:31, gbn:61 and abt:37 


