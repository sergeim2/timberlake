#
# This is the default '.cshrc' file.  Things that go in here
# effect the csh and the tcsh command interpreters. 
# Whenever a new tcsh is started, tcsh automatically sources
# /etc/csh.cshrc, and then sources this file.
#
# For more information do the command 'man csh' and 'man tcsh'
#
set history=50

# Set a default prompt that contains the hostname.  
# See tcsh man page for more options.
set prompt="%m {%~} > "

# Turn off core dumps.
limit coredumpsize 0

