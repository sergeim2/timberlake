package pets;

class ABird extends APet implements IBird {

  private String noise;

  public ABird(String name) {

    super(name);
    this.noise = "tweet! tweet!";
  }

  @Override
  public String makeNoise() {

    return noise;
  }

  @Override
  public boolean playsWith(IBird other) {

    say("-- I will play with other birds!");
    return true;
  }

  @Override
  public boolean playsWith(ICat other) {

    say("-- Yikes! I will NEVER play with cats!");
    return false;
  }
}