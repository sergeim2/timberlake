package pets;

class ACat extends APet implements ICat {

  private String noise;

  public ACat(String name) {

    super(name);
    this.noise = "meow! meow!";
  }

  @Override
  public String makeNoise() {

    return noise;
  }

  @Override
  public boolean playsWith(IBird other) {

    say("-- Birds are my favorite!");
    return true;
  }

  @Override
  public boolean playsWith(ICat other) {

    say("-- I will play with other cats!");
    return true;
  }

  @Override
  public boolean playsWith(IDog other) {

    say("-- Yikes! I will NEVER play with dogs!");
    return false;
  }
}