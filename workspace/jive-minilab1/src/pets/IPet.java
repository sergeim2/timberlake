package pets;

public interface IPet {

  public String getName();

  public String makeNoise();

  public boolean playsWith(IBird other);

  public boolean playsWith(ICat other);

  public boolean playsWith(IDog other);

  public boolean playsWith(IPet other);
}