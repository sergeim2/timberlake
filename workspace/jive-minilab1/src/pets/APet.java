package pets;

abstract class APet implements IPet {

  private String name;

  public APet(String name) {

    this.name = name;
  }

  @Override
  public String getName() {

    return name;
  }

  @Override
  public boolean playsWith(IBird other) {

    say("-- I don't know how to play with a bird.");
    return false;
  }

  @Override
  public boolean playsWith(ICat other) {

    say("-- I don't know how to play with a cat.");
    return false;
  }

  @Override
  public boolean playsWith(IDog other) {

    say("-- I don't know how to play with a dog.");
    return false;
  }

  @Override
  public boolean playsWith(IPet other) {

    say("-- I don't know what this pet is. It makes a strange noise: " + other.makeNoise());
    return true;
  }

  protected void say(String value) {

    System.out.println(getName() + " says:");
    System.out.println(value);
  }
}