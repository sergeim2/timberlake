package pets;

class ADog extends APet implements IDog {

  private String noise;

  public ADog(String name) {

    super(name);
    this.noise = "woof! woof!";
  }

  @Override
  public String makeNoise() {

    return noise;
  }

  @Override
  public boolean playsWith(ICat other) {

    say("-- Cats are my favorite!");
    return true;
  }

  @Override
  public boolean playsWith(IDog other) {

    say("-- I will play with other dogs!");
    return true;
  }
}