package pets;

public class Task {

  public static void main(String[] args) {

    IBird bird = new ABird("Quackly");
    ICat cat = new ACat("Julius");
    IDog dog = new ADog("McBarker");

    int c1 = playBird(bird, cat, dog);
    System.out.println();
    System.out.println("play count #1: " + c1);
    System.out.println("----------------");
    System.out.println();

    int c2 = playCat(bird, cat, dog);
    System.out.println();
    System.out.println("play count #2: " + c2);
    System.out.println("----------------");
    System.out.println();
  }

  private static int playBird(IPet bird, IPet cat, IPet dog) {

    int playCount = 0;
    if (bird.playsWith(cat)) {
      playCount++;
    }
    if (bird.playsWith(dog)) {
      playCount++;
    }
    return playCount;
  }

  private static int playCat(IPet bird, IPet cat, IDog dog) {

    int playCount = 0;
    if (cat.playsWith(bird)) {
      playCount++;
    }
    if (cat.playsWith(dog)) {
      playCount++;
    }
    return playCount;
  }
}