package user_interface;
import java.util.Scanner;
import game_model.Board;

import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


import user_interface.event_handlers.ButtonEventHandler;

public class GraphicalUserInterface implements Runnable {
	
	private int _s;
	private String _p1;
	private String _p2;
	public GraphicalUserInterface(int s, String p1, String p2){
		/*
		Scanner sc = new Scanner(System.in);
		System.out.println("\nPlease Enter Board Size:");
		s=sc.nextInt();
		System.out.println("\nPlease Enter first players name:");
		p1=sc.next();
		System.out.println("\nPlease Enter second players name:");
		p2=sc.next();
		*/
		_s=s;
		_p1=p1;
		_p2=p2;
		}
	
	@Override public void run() {
		JFrame window = new JFrame("TicTacToe");
		GridLayout layout = new GridLayout(_s,_s);
		window.setLayout(layout);
		Board board = new Board(_s,_p1,_p2);
//		JPanel panel = new JPanel();
//		window.add(panel);
//		
		JFrame window2 = new JFrame("Red Players Turn");
		GridLayout layout2 = new GridLayout();
		window2.setLayout(layout2);
		JPanel panel2 = new JPanel();
		window2.add(panel2);
		JButton l1 = new JButton(_p1);
		JButton l2 = new JButton(_p2);
		panel2.add(l1);
		panel2.add(l2);
		
		
		//b.addActionListener(new ButtonEventHandler(b, board));
		//panel.add(b);
	
		
		for(int row=0;row<_s;row++){
			for(int col=0;col<_s;col++){
				JPanel jp=new JPanel();
				window.add(jp);
				JButton b=new JButton("(" + row + "," + col +")");
				b.addActionListener(new ButtonEventHandler(b,board, row, col, l1, l2));
				jp.add(b);
			}
		
	}
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);
		window2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window2.pack();
		window2.setVisible(true);
		
	}
	

}
