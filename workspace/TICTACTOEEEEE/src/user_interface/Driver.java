package user_interface;

import javax.swing.SwingUtilities;

public class Driver {

	public static void main(String[] args) {
		//args[0]="5";
		//args[1]="dan";
		//args[2]="jack";
		SwingUtilities.invokeLater(new GraphicalUserInterface(Integer.parseInt(args[0]), args[1], args[2]));
       
	}
}
