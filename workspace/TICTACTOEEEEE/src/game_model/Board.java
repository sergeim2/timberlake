package game_model;

import java.util.LinkedList;
import java.util.Scanner;
import interfaces.Listener;

public class Board {
	private int _s;
	private String _pp1;
	private String _pp2;
	private String _player1;
	private String _player2;
	private LinkedList<LinkedList<String>> _board;
	private Listener _listener;
	
	public Board(int s,String p1,String p2)
	{
		
	_s=s;
	_player1=p1;
	_player2=p2;
	_board = new LinkedList<LinkedList<String>>(); 
	for(int e=0;e<_s;e++)
	{
		LinkedList<String> innerLinkedList=new LinkedList<String>();
		for(int r=0;r< _s;r++){
			innerLinkedList.add("-");
			
		}
		_board.add(innerLinkedList);
	}	
		
	_listener = null;
	_pp1= "X";
	_pp2= "O";
	}

    //BoardSize= sc.nextInt();
	
	
	//Scanner sc = new Scanner(System.in);

	
	
	
	public String checkWinner(Board b){
		String winner="no one";
		int i=5;
		int p1=0;
		int p2=0;
		String blahx="X";
		String blaho="O";
		//for rows
		for(int row=0;row<_s;row++){
			p1=0;
			p2=0;
			for(int col=0;col<_s;col++){
				
			if(_board.get(row).get(col)=="X")
			{
			p1++;
			}
			if(_board.get(row).get(col)=="O")
			{
				p2++;
				}
	
			}
			
			
		
		
		
		if(p1==_s)
		{
			winner=blahx;
			return winner;
		}
		if(p2==_s)
		{
			winner=blaho;
			return winner;
		}
		
		
	}
		
		//for cols
		for(int row=0;row<_s;row++){
			p1=0;
			p2=0;
			for(int col=0;col<_s;col++){
			if(_board.get(col).get(row)=="X")
			p1++;
			if(_board.get(col).get(row)=="O")
				p2++;
	}
			
		
		
		
		if(p1==_s)
		{
			winner=blahx;
			return winner;
		}
		if(p2==_s)
		{
			winner=blaho;
			return winner;
		}
		
	}
		p1=0;
		p2=0;
		//for diagnal 1 \ <---- this one
		for(int row=0;row<_s;row++){
			if(_board.get(row).get(row)=="X")
			p1++;
			if(_board.get(row).get(row)=="O")
				p2++;
	
			
		
		
		
		if(p1==_s)
		{
			winner=blahx;
			return winner;
		}
		else if(p2==_s)
		{
			winner=blaho;
			return winner;
		}
    }
		
	
		//for diagnal 2 / <---- this one
		p1=0;
		p2=0;
		for(int row=0;row<_s;row++){
			i--;
			if(_board.get(row).get(i)=="X")
			p1++;
			if(_board.get(row).get(i)=="O")
				p2++;
		
		
		if(p1==_s)
		{
			winner=blahx;
			return winner;
		}
		else if(p2==_s)
		{
			winner=blaho;
			return winner;
		}
		
		
	}
		
	return winner;

}
	

//		if()
//			
//		else
//		return 
//	}
	
	public String toString(){
		String s="";
		for(int y=0; y<_s;y++){
			LinkedList<String> row= _board.get(y);
			for(int x=0;x<_s;x++){
			s=s+row.get(x);
			
			}
			s=s + '\n';
	}
		return s;
			}
	public String CurrentPlayer(){
		
		
		
		return _pp1;
	
	}
	
	
	public void put(int col, int row) {
		
		_board.get(row).set(col, _pp1);
		_listener.notifyListener();
		_listener = null;
		String temp= _pp1;
		_pp1=_pp2;
		_pp2=temp;
		System.out.println(toString());
		
	
		
	}
	
	public void setListener(Listener listener) {
		_listener = listener;
		
	}
}
