package graphics;

import javax.swing.JFrame;
import javax.swing.JButton;

public class Example {

	public Example() {
		JFrame frame = new JFrame();
		
		JButton b = new JButton("Button");
		
		frame.add(b);
		
		JButton b2 = new JButton("Click this!");
		/*	Adding this button replaces the old one since we
			don't have a LayoutManager. See BasicExample for
			an example of how to use a FlowLayout
		*/
		frame.add(b2);  
		
		frame.pack();
		frame.setVisible(true);
		
	}

}
