package graphics;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GridLayout25Listener implements ActionListener{

	java.awt.Container _container;
	
	public GridLayout25Listener(Container c) {
		_container = c;
	}

	public void actionPerformed(ActionEvent e) {
		
		_container.setLayout(new GridLayout(2,5));
		_container.doLayout();
		
	}

}
