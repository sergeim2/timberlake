package graphics;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FlowLayoutListener implements ActionListener{

	java.awt.Container _container;
	
	public FlowLayoutListener(Container c) {
		_container = c;
	}

	public void actionPerformed(ActionEvent e) {
		
		_container.setLayout(new FlowLayout());
		_container.doLayout();
		
	}

}
