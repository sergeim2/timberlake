package graphics;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GridLayoutListener implements ActionListener{

	java.awt.Container _container;
	
	public GridLayoutListener(Container c) {
		_container = c;
	}

	public void actionPerformed(ActionEvent e) {
		
		_container.setLayout(new GridLayout());
		_container.doLayout();
		
	}

}
