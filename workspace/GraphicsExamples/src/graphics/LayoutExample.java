package graphics;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LayoutExample {

	public LayoutExample() {
		
		// create a JFrame
		JFrame frame = new JFrame("Layout Examples");
		
		// create a JPanel that will be the Content Pane 
		JPanel mainPanel = new JPanel();
		
		// add the mainPanel to the Frame
		frame.add(mainPanel);
		
		// Create some Buttons
		JButton buttonOne = new JButton("One");
		JButton buttonTwo = new JButton("Two");
		JButton buttonThree = new JButton("Three");
		
		// Create some Labels
		JLabel labelOne = new JLabel("Label One");
		JLabel labelTwo = new JLabel("Label Two");
		JLabel labelThree = new JLabel("Label Three");

		// Set the background color so that we can see where the panel is
		mainPanel.setBackground(java.awt.Color.CYAN);
		
		// Showing the BorderLayout first
		mainPanel.setLayout(new BorderLayout());
		
		// Add all of the components, with the BorderLayout constraints
		mainPanel.add(buttonOne,BorderLayout.NORTH);
		mainPanel.add(buttonTwo,BorderLayout.SOUTH);
		mainPanel.add(buttonThree,BorderLayout.EAST);

		mainPanel.add(labelOne,BorderLayout.WEST);
		mainPanel.add(labelTwo,BorderLayout.CENTER);
		
		// The defailt location in BorderLayout is CENTER
		// So labelThree will replace labelTwo in the Panel,
		// although labelTwo will still be considered to be "added"
		// to the Panel
		mainPanel.add(labelThree);
		
		// Create a second frame to hold the Buttons 
		JFrame layoutFrame = new JFrame("Layout Switching Buttons");
		
		layoutFrame.setLayout(new GridLayout(0,1,5,5));
		
		JButton flowButton = new JButton("Flow Layout");
		flowButton.addActionListener(new FlowLayoutListener(mainPanel));
		
		JButton boxLayoutX = new JButton("BoxLayout (X-Axis)");
		boxLayoutX.addActionListener(new BoxLayoutXListner(mainPanel));
		
		JButton boxLayoutY = new JButton("BoxLayout (Y-Axis)");
		boxLayoutY.addActionListener(new BoxLayoutYListner(mainPanel));
		

		JButton gridButton = new JButton("GridLayout()");
		gridButton.addActionListener(new GridLayoutListener(mainPanel));
		
		JButton gridButton25 = new JButton("GridLayout(2,5)");
		gridButton25.addActionListener(new GridLayout25Listener(mainPanel));
		
		JButton gridButton52 = new JButton("GridLayout(5,2)");
		gridButton52.addActionListener(new GridLayout52Listener(mainPanel));
		
		
		
		
		layoutFrame.add(flowButton);
		layoutFrame.add(boxLayoutX);
		layoutFrame.add(boxLayoutY);
		layoutFrame.add(gridButton);
		layoutFrame.add(gridButton25);
		layoutFrame.add(gridButton52);
		
		
		
		
		frame.pack();
		frame.setVisible(true);
		
		layoutFrame.pack();
		layoutFrame.setVisible(true);
	}

}
