package graphics;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

public class BoxLayoutYListner implements ActionListener {
	
	private Container _container;
	
	public BoxLayoutYListner(Container c){
		_container = c;
	}

	public void actionPerformed(ActionEvent e) {
		_container.setLayout(new BoxLayout(_container,BoxLayout.Y_AXIS));
		_container.doLayout();
		
	}

}
