package graphics;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Container;
import java.awt.FlowLayout;

public class BasicExample {

	public BasicExample() {
		
		// Create the JFrame
		JFrame frame = new JFrame();
		
		// Create a JLabel to add to the Frame
		JLabel label = new JLabel("This is a JLabel");
		
		// Change the Background Color so that we can identify the Label
		label.setBackground(java.awt.Color.GREEN);
		label.setOpaque(true);
		
		// Create a JButton that we can add to the Frame
		JButton button = new JButton("Push me!");
		
		// Change the background Color so that we can identify the Button
		button.setBackground(java.awt.Color.ORANGE);
		
		// Add a LayoutManager so that we can see all of our components
		frame.setLayout(new FlowLayout());
		
		//Two options for adding to the Frame:
		//	Option One: explicitly 
		Container contentPane = frame.getContentPane();
		contentPane.add(label);
		
		//	Option Two: implicitly through the frame reference
		frame.add(button);
		
		// Important steps for every Frame
		frame.pack();
		frame.setVisible(true);
		
		// Optional -- Close the program when we close the JFrame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
