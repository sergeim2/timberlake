package lab2;

public class EcoSystem {

	public EcoSystem() {
		
		example1.Terrarium t;
		t=new example1.Terrarium();
		example1.Ant a;
		example1.Ant b;
		example1.Ant c;
		example1.Butterfly d;
		example1.Butterfly e;
		example1.Caterpillar f;
		example1.Caterpillar g;
		a=new example1.Ant();
		b=new example1.Ant();
		c=new example1.Ant();
		e=new example1.Butterfly();
		d=new example1.Butterfly();
		f=new example1.Caterpillar();
		g=new example1.Caterpillar();
		t.add(a);
		t.add(b);
		t.add(c);
		t.add(d);
		t.add(e);
		t.add(f);
		t.add(g);
		a.start();
		b.start();
		d.start();
		f.start();
		g.start();
		
	}
	
}
