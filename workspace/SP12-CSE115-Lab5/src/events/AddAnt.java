package events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddAnt implements ActionListener {
	private example1.Terrarium _t;
public AddAnt(example1.Terrarium t){
	_t=t;
}


@Override
public void actionPerformed(ActionEvent e) {
	example1.Ant a;
	a=new example1.Ant();
	_t.add(a);
	a.start();
}
}