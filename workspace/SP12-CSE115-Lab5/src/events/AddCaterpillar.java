package events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddCaterpillar implements ActionListener {
	private example1.Terrarium _t;
public AddCaterpillar(example1.Terrarium t){
	_t=t;
}


@Override
public void actionPerformed(ActionEvent e) {
	example1.Caterpillar a;
	a=new example1.Caterpillar();
	_t.add(a);
	a.start();
}
}
