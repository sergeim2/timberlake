package events;


import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Lab5 {
	public Lab5(){
	example1.Terrarium t=new example1.Terrarium(); //make a new terrarium 
	JFrame myframe=new JFrame("CSE115 – Lab 5 – Sergei Mellow");
	JButton antbutton=new JButton("add Ant");
	JButton butterflybutton=new JButton("add Butterfly");
	JButton caterpillarbutton=new JButton("add Caterpillar");
	antbutton.addActionListener(new AddAnt(t));
	butterflybutton.addActionListener(new AddButterly(t));
	caterpillarbutton.addActionListener(new AddCaterpillar(t));
	myframe.add(antbutton,BorderLayout.WEST);
	myframe.add(butterflybutton,BorderLayout.EAST);
	myframe.add(caterpillarbutton,BorderLayout.SOUTH);
	myframe.pack();
	myframe.setVisible(true);
	}
}
