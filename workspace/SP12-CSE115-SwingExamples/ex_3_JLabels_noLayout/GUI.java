package ex_3_JLabels_noLayout;

public class GUI {

	private javax.swing.JFrame _mainWindow;
	
	public GUI() {
		_mainWindow = new javax.swing.JFrame("Swing example #3");
		_mainWindow.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		javax.swing.JLabel label1 = new javax.swing.JLabel("Some text on label #1");
		_mainWindow.getContentPane().add(label1);
		javax.swing.JLabel label2 = new javax.swing.JLabel("Some text on label #2");
		_mainWindow.getContentPane().add(label2);
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
