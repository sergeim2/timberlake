package ex_4_staticLayout;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class GUI {

	private JFrame _mainWindow;
	
	public GUI() {
		_mainWindow = new JFrame("A simple application");
		_mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = _mainWindow.getContentPane();

		LayoutManager layout1 = new BoxLayout(pane,BoxLayout.X_AXIS);
		LayoutManager layout2 = new BoxLayout(pane,BoxLayout.Y_AXIS);
		LayoutManager layout3 = new GridLayout(0,2);
		LayoutManager layout4 = new FlowLayout();
		
		// CHANGE MANUALLY WHICH LAYOUT MANAGER IS BEING USED - THEN RE-RUN
		// ex_7_dynamicLayout SHOWS THIS DONE INTERACTIVELY/DYNAMICALLY
		pane.setLayout(layout4);
		
		pane.setBackground(java.awt.Color.WHITE);
		pane.add(new JLabel("Some text on label #1"));
		pane.add(new JLabel("Some text on label #2"));
		pane.add(new JLabel("Some text on label #3"));
		pane.add(new JLabel("Some text on label #4"));
		pane.add(new JLabel("Some text on label #5"));
		pane.add(new JLabel("Some text on label #6"));
		pane.add(new JLabel("Some text on label #7"));
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
