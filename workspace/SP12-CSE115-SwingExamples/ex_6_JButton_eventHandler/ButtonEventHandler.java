package ex_6_JButton_eventHandler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonEventHandler implements ActionListener {

	private javax.swing.JButton _button;
	
	public ButtonEventHandler(javax.swing.JButton button) {
		_button = button;
	}
	
	public void actionPerformed(ActionEvent e) {
		java.lang.System.out.println("\tStop\n\tthat!");
		_button.setBackground(java.awt.Color.RED);
	}
}
