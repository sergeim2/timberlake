package ex_6_JButton_eventHandler;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GUI {

	private JFrame _mainWindow;
	private JButton _button;
	
	public GUI() {
		_mainWindow = new JFrame("A simple application");
		_mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_button = new JButton("Some text on a button");
// If we are running on OS-X, we need the following line:
		_button.setOpaque(true);	

		_button.addActionListener(new ButtonEventHandler(_button));
		_mainWindow.getContentPane().add(_button);
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
