package JButtonEventHanding;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class EventHandler implements ActionListener {
	
	private JButton _button;
	
	public EventHandler(JButton b) {
		_button = b;
	}

	@Override public void actionPerformed(ActionEvent e) {
		_button.setBackground(java.awt.Color.CYAN);
		System.out.println("Good morning!");
	}
}
