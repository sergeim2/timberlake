package JButtonEventHanding;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GUI {

	private JFrame _mainWindow;
	
	public GUI() {
		_mainWindow = new JFrame("A simple application");
		_mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton button = new JButton("Some text on a button");
		button.setOpaque(true);
		EventHandler eh = new EventHandler(button);
		button.addActionListener(eh);
		_mainWindow.getContentPane().add(button);
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
