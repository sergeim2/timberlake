package ex_7_dynamicLayoutEventHandling;

import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class ButtonHandler implements ActionListener {

	private LayoutManager _layout;
	private Container _container;
	private JFrame _frame;
	
	public ButtonHandler(LayoutManager layout, Container container, JFrame frame) {
		_layout = layout;
		_container = container;
		_frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		_container.setLayout(_layout);
		_container.validate();
		_frame.pack();
	}
}
