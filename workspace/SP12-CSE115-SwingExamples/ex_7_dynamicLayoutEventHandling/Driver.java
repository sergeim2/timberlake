package ex_7_dynamicLayoutEventHandling;

/**
 * TODO: fill in comments for Driver
 *
 * @author <a href="mailto:alphonce@cse.buffalo.edu">Carl G. Alphonce</a>
 *
 * Created on: Oct 10, 2006
 *
 */
public class Driver {
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new GUI());
	}
}
