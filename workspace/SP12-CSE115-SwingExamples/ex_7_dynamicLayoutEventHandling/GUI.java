package ex_7_dynamicLayoutEventHandling;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI implements Runnable{

	private JFrame _mainWindow;
	
	public GUI() {}
	
	@Override
	public void run() {
		_mainWindow = new JFrame("A simple application");
		_mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = _mainWindow.getContentPane();

		pane.setLayout(new BorderLayout());
		
		JPanel _labelPanel = new JPanel();
		_labelPanel.setLayout(new BoxLayout(_labelPanel, BoxLayout.X_AXIS));
		
		JPanel _buttonPanel = new JPanel();
		_buttonPanel.setLayout(new BoxLayout(_buttonPanel,BoxLayout.Y_AXIS));

		pane.add(_buttonPanel,BorderLayout.WEST);
		pane.add(_labelPanel,BorderLayout.CENTER);
		
		_labelPanel.setBackground(java.awt.Color.WHITE);
		
		JLabel l1 = new JLabel("Some text on label #1");
		l1.setBackground(java.awt.Color.RED);
		l1.setOpaque(true);
		JLabel l2 = new JLabel("Some text on label #2");
		l2.setBackground(java.awt.Color.BLUE);
		l2.setOpaque(true);
		
		_labelPanel.add(l1);
		_labelPanel.add(l2);
		_labelPanel.add(new JLabel("Some text on label #3"));
		_labelPanel.add(new JLabel("Some text on label #4"));
		_labelPanel.add(new JLabel("Some text on label #5"));
		_labelPanel.add(new JLabel("Some text on label #6"));
		_labelPanel.add(new JLabel("Some text on label #7"));

		JButton aButton;
		aButton = new JButton("BoxLayout along X-axis");
		aButton.addActionListener(new ButtonHandler(new BoxLayout(_labelPanel,BoxLayout.X_AXIS), _labelPanel, _mainWindow));
		_buttonPanel.add(aButton);

		aButton = new JButton("BoxLayout\n along Y-axis");
		aButton.setOpaque(true);
		aButton.setBackground(java.awt.Color.GREEN);
		aButton.addActionListener(new ButtonHandler(new BoxLayout(_labelPanel,BoxLayout.Y_AXIS), _labelPanel, _mainWindow));
		_buttonPanel.add(aButton);

		aButton = new JButton("GridLayout");
		aButton.addActionListener(new ButtonHandler(new GridLayout(0,2), _labelPanel, _mainWindow));
		_buttonPanel.add(aButton);

		aButton = new JButton("FlowLayout");
		aButton.addActionListener(new ButtonHandler(new FlowLayout(), _labelPanel, _mainWindow));
		_buttonPanel.add(aButton);
		
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
