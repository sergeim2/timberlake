package ex_2_JLabel_tooltip;

public class GUI {

	private javax.swing.JFrame _mainWindow;

	public GUI() {
		_mainWindow = new javax.swing.JFrame("Swing example #2");
		_mainWindow.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		javax.swing.JLabel label = new javax.swing.JLabel("Some text on a label");
		label.setToolTipText("I am a tooltip!");
//		_mainWindow.add(label);
// The add(c) method on a JFrame is a convenience method
// which adds c to the JFrame's content pane:
		_mainWindow.getContentPane().add(label);
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
