package loops;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ButtonHandler implements ActionListener {

	JButton _button;
	
	public ButtonHandler(JButton b) {
		_button = b;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Button text is "+_button.getSize());
	}

}
