package loops;

import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class UsingLoops {
	private javax.swing.JFrame _window;
	
	public UsingLoops() {
		_window = new javax.swing.JFrame();
		
		JPanel buttonPanel = new JPanel();
		_window.add(buttonPanel);
		
		BoxLayout layout = new BoxLayout(buttonPanel, BoxLayout.X_AXIS);
		buttonPanel.setLayout(layout);

		ArrayList<String> list = new ArrayList<String>();		
		list.add("Monday");
		list.add("Tuesday");
		list.add("Wednesday");
		list.add("Thursday");
		list.add("Friday");
		list.add("Saturday");
		list.add("Sunday");
		
		for(String t : list) {
			createButton(buttonPanel, t);
		}
		
		_window.pack();
		_window.setVisible(true);
	}
	
	public JButton createButton(JPanel buttonPanel, String text) {
		JButton button1 = new JButton(text);
		button1.addActionListener(new ButtonHandler(button1));
		button1.setBackground(java.awt.Color.CYAN);
		buttonPanel.add(button1);
		return button1;
	}

	
	public static void main(String [] args) {
		new UsingLoops();
	}

}
