package floating_point;

public class DoubleTrouble {
	
	public void inexact() {
		double a = 0.0;
		for (int i=0; i<10; i++) {
			a = a + 0.1;
			System.out.println(a);
		}
	}
	
	public void notEqual() {
		double a = 0.3;
		double b = 0.1 + 0.2;
		System.out.println("a has value "+a);
		System.out.println("b has value "+b);
		System.out.println("a == b has value " + (a == b));
		double epsilon = 0.0000000000000001;
		System.out.println("|a-b| < epsilon has value " + (Math.abs(a-b) < epsilon));
	}
	
	public void associativityCommutativity() {
		// commutativity L+s+s=s+s+L
		double s = 1e-10;
		double l = 1e+10;
		double ls = l;
		System.out.println("before loop, ls: "+ls);
		for (int i=0; i<10000; i++) { ls = ls + s; }
		System.out.println(" after loop, ls: "+ls);
		double sl = 0.0;
		System.out.println("before loop, sl: "+sl);
		for (int i=0; i<10000; i++) { sl = sl + s; }
		sl = sl + l;
		System.out.println(" after loop, sl: "+sl);
		// associativity a+(b+c)=(a+b)+c
		// l + ( s + ( s + ... s ) ) .ne. (((l + s) + s) + ... + s)
	}
}
