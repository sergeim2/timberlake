package collections.mainevent;

public class Dog implements Pet {
	
	private String _name;
	
	public Dog(String n) {
		_name = n;
	}
	
	@Override public String makeNoise() {
		return _name + ", I say 'woof'";
	}

}
