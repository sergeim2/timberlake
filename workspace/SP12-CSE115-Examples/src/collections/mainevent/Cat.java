package collections.mainevent;

public class Cat implements Pet {
	
	private String _name;
	
	public Cat(String n) {
		_name = n;
	}
	
	@Override
	public String makeNoise() {
		return _name + ", I say 'meow'.";
	}

}
