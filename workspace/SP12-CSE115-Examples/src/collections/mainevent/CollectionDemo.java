package collections.mainevent;

import java.util.LinkedList;

public class CollectionDemo {
	public CollectionDemo() {
		LinkedList<Pet> pets;
		pets = new LinkedList<Pet>();
		pets.add(new Cat("Fluffy"));
		pets.add(new Cat("Whiskers"));
		pets.add(new Cat("Garfield"));
		pets.add(new Cat("Bubbles"));		
		pets.add(new Dog("Clifford"));
		pets.add(new Dog("Fido"));
		pets.add(new Dog("Rover"));
		pets.add(new Dog("Lassie"));
		pets.add(new Dog("Spot"));
		pets.add(new Fish());
		for (Pet p : pets) {
			System.out.println(p.makeNoise());
		}	
	}
	
	public static void main(String[] args) {
		new CollectionDemo();
	}
}
