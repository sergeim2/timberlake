package collections.mainevent;

public class Driver {
	
	public static void main(String[] args) {
		java.util.Collection<String> coll;
		coll = new java.util.LinkedList<String>();
		coll.add("Fred");
		coll.add("Wilma");
		coll.add("Pebbles");
		coll.add("Dino");
		coll.add("Fred");
		System.out.println("The collection: " + coll);
		java.util.Iterator<String> it;
		it = coll.iterator();
		String cs = "[ ";
		while (it.hasNext()   ) {
			String s = it.next();
			cs = cs + s.toString() + " ";
		}
		cs = cs + "]";
		System.out.println("Our \"toString\" produces \\n: " + cs);
	}
	
}
