package collections.mainevent;

public class Fish implements Pet {
	@Override public String makeNoise() {
		return "glub, glub, glub...";
	}
}
