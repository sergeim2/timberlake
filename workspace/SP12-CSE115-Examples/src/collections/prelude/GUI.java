package collections.prelude;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GUI {

	private JFrame _mainWindow;
	private JButton _button;
	
	public GUI() {
		_mainWindow = new JFrame("A simple application");
		_mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_button = new JButton("Button 1");
		_button.addActionListener(new ButtonEventHandler1());
		_button.addActionListener(new ButtonEventHandler2(_button));
		_button.addActionListener(new ButtonEventHandler2(_button));

		_mainWindow.getContentPane().add(_button);
		_mainWindow.pack();
		_mainWindow.setVisible(true);
	}
}
