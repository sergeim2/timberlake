package collections.prelude;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ButtonEventHandler2 implements ActionListener {
	private JButton _button;
	
	public ButtonEventHandler2(JButton b) {
		_button = b;
	}
	
	public void actionPerformed(ActionEvent e) {
		java.lang.System.out.println("Output of ButtonEventHandler2");
		_button.removeActionListener(this);
	}
}
