package lab2;

public class EcoSystem {
	
	private example1.Terrarium _t;
	private example1.Terrarium _s;
	
	public EcoSystem() {
		_t = new example1.Terrarium();
		addTwoCaterpillars();
	}
	
	public example1.Terrarium getTerrarium() {
		return _t;
	}
	
	public void addTwoCaterpillars() {
		example1.Caterpillar c1 = new example1.Caterpillar();
		example1.Caterpillar c2 = new example1.Caterpillar();
		_t.add(c1);
		_t.add(c2);
		c1.start();
		c2.start();
	}

	public void addOneOfEach() {
		example1.Caterpillar c1 = new example1.Caterpillar();
		example1.Butterfly b1 = new example1.Butterfly();
		example1.Ant a1 = new example1.Ant();
		_t.add(c1);
		_t.add(b1);
		_t.add(a1);
		a1.start();
		b1.start();
		c1.start();
	}

}
