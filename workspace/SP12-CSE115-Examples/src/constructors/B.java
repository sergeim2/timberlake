package constructors;

import javax.swing.JButton;

public class B extends A {
	public B() {
		super(new JButton(),"Fred");
		System.out.println("Constructor: B()");
	}

}
