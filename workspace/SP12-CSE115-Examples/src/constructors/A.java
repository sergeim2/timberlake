package constructors;

import javax.swing.JButton;

public class A {
	public A() {
		System.out.println("Constructor: A()");
	}
	public A(String n) {
		System.out.println("Constructor: A(String)");
	}
	public A(String n, JButton b) {
		System.out.println("Constructor: A(String,JButton)");
	}
	public A(JButton b, String n) {
		System.out.println("Constructor: A(JButton,String)");
	}

}
