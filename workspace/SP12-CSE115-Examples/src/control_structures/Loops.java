package control_structures;

import java.util.Iterator;
import java.util.LinkedList;

import collections.mainevent.Cat;
import collections.mainevent.Dog;
import collections.mainevent.Fish;
import collections.mainevent.Pet;

public class Loops {
	
	/**
	 * This method shows how to iterate through each element of a LinkedList
     * using a foreach loop.  The foreach loop is "syntactic sugar"; it is
     * equivalent to a for loop (see forLoopIteratorVersion below).
	 * @param pets
	 */
	public void foreachLoop(LinkedList<Pet> pets) {
		for (Pet p : pets) {
			System.out.println(p.makeNoise());
		}	
	}

	/**
	 * This method shows how to iterate through each element of a LinkedList
     * using a while loop controlled by an explicit iterator.
	 * @param pets
	 */
	public void whileLoopIteratorVersion(LinkedList<Pet> pets) {
		Iterator<Pet> it = pets.iterator();
		while (it.hasNext()) {
			Pet p = it.next();
			System.out.println(p.makeNoise());
		}
	}

	/**
	 * This method shows how to iterate through each element of a LinkedList
     * using a for loop controlled by an explicit iterator.
	 * @param pets
	 */
	public void forLoopIteratorVersion(LinkedList<Pet> pets) {
		for (Iterator<Pet> it = pets.iterator(); it.hasNext(); /* empty statement */ ) {
			Pet p = it.next();  // update iterator in body, so p has a value in println statement
			System.out.println(p.makeNoise());
		}
	}

	/**
	 * This method shows how to iterate through each element of a LinkedList
     * using a while loop controlled by an explicit index.
     * Each element of a LinkedList has an index; elements in a LinkedList
     * of size() N have indices ranging from 0 to N-1. 
	 * @param pets
	 */
	public void whileLoopIndexVersion(LinkedList<Pet> pets) {
		int i = 0;
		while (i < pets.size()) {
			Pet p = pets.get(i);
			System.out.println(p.makeNoise());
			i = i + 1;
		}	
	}

	/**
	 * This method shows how to iterate through each element of a LinkedList
     * using a for loop controlled by an explicit index.
     * Each element of a LinkedList has an index; elements in a LinkedList
     * of size() N have indices ranging from 0 to N-1. 
	 * @param pets
	 */
	public void forLoopIndexVersion(LinkedList<Pet> pets) {
		for (int i = 0; i < pets.size(); i = i + 1) {
			Pet p = pets.get(i);
			System.out.println(p.makeNoise());
		}	
	}

	public Loops() {
		// Create a LinkedList containing some Pet objects.
		// Remember: Cat implements Pet, Dog implements Pet, and
        //           Fish implements Pet
		LinkedList<Pet> pets;
		pets = new LinkedList<Pet>();
		pets.add(new Cat("Fluffy"));
		pets.add(new Cat("Whiskers"));
		pets.add(new Cat("Garfield"));
		pets.add(new Cat("Bubbles"));
		pets.add(new Dog("Clifford"));
		pets.add(new Dog("Fido"));
		pets.add(new Dog("Rover"));
		pets.add(new Dog("Lassie"));
		pets.add(new Dog("Spot"));
		pets.add(new Fish());
		
		// Try out the various loops; they should each print out the
		// same result.
		System.out.println("Trying the foreach loop (which uses an implicit iterator):");
		foreachLoop(pets);
		System.out.println("Trying the while loop with an explicit iterator:");
		whileLoopIteratorVersion(pets);
		System.out.println("Trying the for loop with an explicit iterator:");
		forLoopIteratorVersion(pets);
		System.out.println("Trying the while loop with an explicit index:");
		whileLoopIndexVersion(pets);
		System.out.println("Trying the for loop with an explicit index:");
		forLoopIndexVersion(pets);
		System.out.println("All done!");
	}
}
