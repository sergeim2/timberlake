package jive;

import java.util.LinkedList;

public class Demo {
	public static void main(String[] args) {
		LinkedList<Student> roster = new LinkedList<Student>();
		Student amy = new Student();
		Student bob = new Student();
		Student sue = new Student();
		Student dan = new Student();
		roster.add(amy);
		roster.add(bob);
		roster.add(sue);
		roster.add(dan);
		roster.add(new Student());
		roster.add(new Student());
		roster.add(new Student());
		roster.add(new Student());
		roster.add(new Student());
		roster.add(new Student());
		amy.addToGrade(30);
		amy.addToGrade(50);
		bob.addToGrade(18);
		bob.addToGrade(27);
		bob.addToGrade(45);
		sue.addToGrade(20);
		sue.addToGrade(22);
		sue.addToGrade(30);
		System.out.println("Using a foreach loop:");
		for (Student s : roster) {
			System.out.println(s.currentGrade());
		}
		System.out.println("Using a for loop:");
		for (int i=0 ; i<roster.size() ; i = i+2 ) {
			System.out.println(roster.get(i).currentGrade());
		}
	}
}
