package jive;

public class Student {
	private int _grade;
	
	public Student() {
		_grade = 0;
	}
	
	public void addToGrade(int x) {
		_grade = _grade + x;
	}
	
	public int currentGrade() {
		return _grade;
	}
}
