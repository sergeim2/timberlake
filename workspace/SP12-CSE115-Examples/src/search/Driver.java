package search;

import java.util.ArrayList;

public class Driver {
	
	public static void main(String[] args) {
		Driver d = new Driver();
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(14);
		list.add(19);
		list.add(27);
		list.add(2);
		list.add(44);
		list.add(51);
		list.add(8);
		list.add(70);
		list.add(42);
		System.out.println("The data is "+list);
		boolean result = d.linearSearchUnsorted(37,list);
		System.out.println("The result is "+result);
    	System.out.println("The smallest value is "+d.smallest(list));
		System.out.println("The index of the smallest value is "+d.indexOfSmallest(list));
		//for (int i=0; i<10; i++) { list.add(i); }
		//for (int i=-2; i<12; i++) { System.out.println("binary search for "+i+" returns "+d.binarySearchSorted(i, list)); }
	}

	/**
	 * This method returns true if target is in list, false otherwise.
	 * Assumption: the list is not empty.
	 * @param target - the value we are searching for
	 * @param list - the collection in which we are searching for target
	 * @return true if target is in list, false otherwise
	 */
	public boolean linearSearchUnsorted(int target, ArrayList<Integer> list) {
		for (int i : list) {
			System.out.println("Comparing "+i+" from list to the target "+target);
			if (i == target) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method returns the smallest value in list.
	 * Assumption: the list is not empty.
	 * @param list - the collection in which we are searching for smallest
	 * @return the smallest from list
	 */
	public int smallest(ArrayList<Integer> list) {
		int hypothesis = list.get(0);
		System.out.println("The initial hypothesis is "+hypothesis);
		for (int i:list) {
			System.out.println("Comparing "+i+" from list to the current hypothesis "+hypothesis);
			if ( i < hypothesis ) {
				System.out.println("Found a smaller value, updating hypothesis");
				hypothesis = i;
			}
		}
		return hypothesis;
	}
	
	public int indexOfSmallest(ArrayList<Integer> list) {
		int hypothesis = 0;
		System.out.println("The initial hypothesis is "+hypothesis);
		for (int i=1; i<list.size(); i=i+1) {
			System.out.println("Comparing value at "+i+", "+list.get(i)+", to value at "+hypothesis+", "+list.get(hypothesis)+".");
			if ( list.get(i) < list.get(hypothesis) ) {
				System.out.println("Found a smaller value, updating hypothesis");
				hypothesis = i;
			}
		}
		return hypothesis;
	}
	
	public boolean linearSearchSorted(int target, ArrayList<Integer> list) {
		for (int i : list) {
			System.out.println("Comparing "+i+" from list to the target "+target);
			if (target == i) {
				return true;
			}
			// if we know the data is sorted from smallest to largest, we can stop early
			else if (target > i) {
				return false;
			}
		}
		return false;
	}
	
	public boolean binarySearchSorted(int target, ArrayList<Integer> list) {
		int l = 0;
		int r = list.size();
		int mid = (l+r)/2;
		while (l < r) {
			     if (target < list.get(mid)) { r = mid; }
			else if (target > list.get(mid)) { l = mid+1; }
			else                             { return true; }
			mid = (l+r)/2;
		}
		return false;
	}
	
	public boolean allSame(ArrayList<Integer> list) {
		int first = list.get(0);
		boolean result = true;  // use && to combine, so use ID for && as initial value
		for (int i=1; i<list.size(); i++) {
			result = result && (first == list.get(i)); // use .equals method with Strings!
		}
		return result;
	}
	
	public boolean oneAllSame(ArrayList<ArrayList<Integer>> grid) {
		boolean result = false;  // use || to combine, so use ID for || as initial value
		for (int i=0; i<grid.size(); i++) {
			result = result || allSame(grid.get(i));
		}
		return result;
	}

}
