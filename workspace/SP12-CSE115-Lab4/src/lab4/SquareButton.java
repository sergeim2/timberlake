package lab4;

public class SquareButton {
	private supportcode.Window _w;
	private ColorHolder _ch;
	public SquareButton(supportcode.Window w, ColorHolder ch) {
		_w=w;
		_ch=ch;	
	}
	public void buttonPressed(){
		supportcode.Square square= new supportcode.Square(_ch);
		square.setColor(_ch.getColor());
		_w.addSquare(square);	
	}
}
