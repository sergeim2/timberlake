package lab4;

public class CircleButton {
	private supportcode.Window _w;
	private ColorHolder _ch;
	public CircleButton(supportcode.Window w, ColorHolder ch){
		_w=w;
		_ch=ch;
		}
	public void buttonPressed(){
		supportcode.Circle circle= new supportcode.Circle(_ch);
		circle.setColor(_ch.getColor());
		_w.addCircle(circle);	
	}
}
