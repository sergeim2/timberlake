package lab4;

public class ColorButton {
	private java.awt.Color _c;
	private ColorHolder _ch;
	public ColorButton(java.awt.Color c, ColorHolder ch) {
		_ch=ch;
		_c=c;
	}
	public void buttonPressed(){
		_ch.setColor(_c);
	}
}
