package lab4;

public class SimpleDraw {
	
	public SimpleDraw() {
		
		//supportcode.Square square= new supportcode.Square(_ch);
		supportcode.Window w=new supportcode.Window();
		ColorHolder ch=new ColorHolder();
		SquareButton sb=new SquareButton(w,ch);
		CircleButton cb=new CircleButton(w,ch);
		ColorButton a=new ColorButton(java.awt.Color.RED,ch);
		ColorButton b=new ColorButton(java.awt.Color.BLUE,ch);
		ColorButton c=new ColorButton(java.awt.Color.GREEN,ch);
		w.addSquareButton(sb);
		w.addCircleButton(cb);
		w.addColorButton(a);
		w.addColorButton(b);
		w.addColorButton(c);
	}
	//public window ()
	
}
