package game_model;

import java.util.LinkedList;

import interfaces.Listener;

public class Board {
	private String _p1;
	private String _p2;
	public static final int BoardSize=3;
	private LinkedList<LinkedList<String>> _board;
	private Listener _listener;
	
	public Board() {
		//String p1=new String("x");
		//String p2=new String("o");
		_board = new LinkedList<LinkedList<String>>(); 
		for(int e=0;e<BoardSize;e++)
		{
			LinkedList<String> innerLinkedList=new LinkedList<String>();
			for(int r=0;r< BoardSize;r++){
				innerLinkedList.add("-");
				
			}
			_board.add(innerLinkedList);
		}	
			
		_listener = null;
		_p1="x";
		_p2="o";
				
	}
	
	public String toString(){
		String s="";
		for(int y=0; y<BoardSize;y++){
			LinkedList<String> row= _board.get(y);
			for(int x=0;x<BoardSize;x++){
			s=s+row.get(x);
			
			}
			s=s + '\n';
	}
		return s;
			}
	public String CurrentPlayer(){
		
		
		
		return _p1;
	
	}
	
	
	public void put(int col, int row) {
		
		_board.get(row).set(col, _p1);
		_listener.notifyListener();
		_listener = null;
		String temp= _p1;
		_p1=_p2;
		_p2=temp;
		System.out.println(toString());
		
	
		
	}
	
	public void setListener(Listener listener) {
		_listener = listener;
		
	}
}
