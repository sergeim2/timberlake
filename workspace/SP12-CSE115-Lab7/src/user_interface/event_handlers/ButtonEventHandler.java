package user_interface.event_handlers;

import game_model.Board;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class ButtonEventHandler implements ActionListener {

	private JButton _button;
	private Board _board;
	private int _r;
	private int _c;
	
	public ButtonEventHandler(JButton button, Board board, int col, int row) {
		_button = button;
		_board = board;
		_r=row;
		_c=col;
		//_board.put(col, row);
	}
	
	
	
	@Override public void actionPerformed(ActionEvent e) {
		_board.setListener(new UpdateListener(_button,_board));
		_board.put(_c,_r);
		
	}

}
