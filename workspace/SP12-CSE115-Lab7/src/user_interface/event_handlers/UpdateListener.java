package user_interface.event_handlers;

import interfaces.Listener;
import game_model.Board;
import javax.swing.JButton;

public class UpdateListener implements Listener {
	//public static int Alter=2;
	private JButton _button;
	private Board _board;
	
	public UpdateListener(JButton button, Board board) {
		
		_button = button;
		_board=board;
		
	
	}
	
	@Override
	public void notifyListener() {
		_button.setText(_board.CurrentPlayer());
		
		/*
		if((Alter%2)==0)
		_button.setText("x");
		else
			_button.setText("0");
			Alter++;
			_board.toString();
			//_board.put()
	}
	*/
	}
}
