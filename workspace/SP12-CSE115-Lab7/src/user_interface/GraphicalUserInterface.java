package user_interface;

import game_model.Board;

import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import user_interface.event_handlers.ButtonEventHandler;

public class GraphicalUserInterface implements Runnable {
	
	@Override public void run() {
		JFrame window = new JFrame("TicTacToe");
		GridLayout layout = new GridLayout(3,3);
		window.setLayout(layout);
		Board board = new Board();
		//JPanel panel = new JPanel();
		//window.add(panel);
		//JButton b = new JButton("BUTTON");
		//b.addActionListener(new ButtonEventHandler(b, board));
		//panel.add(b);
	
		
		for(int row=0;row<3;row++){
			for(int col=0;col<3;col++){
				JPanel jp=new JPanel();
				window.add(jp);
				JButton b=new JButton("(" + row + "," + col +")");
				b.addActionListener(new ButtonEventHandler(b,board, row, col));
				jp.add(b);
			}
		
	}
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);	
		
	}
	

}
