package lab6.tools;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;

import cse115.graphics.DrawingCanvas;

public class SquareTool implements ITool {
	private DrawingCanvas _dc;
	private Cursor _cursor;
	public SquareTool(DrawingCanvas dc){
		_dc=dc;
		_cursor=_dc.makeCustomCursor("src/IMAGES/Square.png");	
	}
	
	

	@Override
	public void apply(Point p) {
		// TODO Auto-generated method stub
		System.out.println("square tool clicked");
		cse115.graphics.Rectangle r=new cse115.graphics.Rectangle();
		r.setColor(java.awt.Color.RED);
		r.setDimension(new Dimension(100,100));
		r.setLocation(p);
		_dc.add(r);

	}

	@Override
	public Cursor getCursor() {
		// TODO Auto-generated method stub
		
		return _cursor;
	}
}
