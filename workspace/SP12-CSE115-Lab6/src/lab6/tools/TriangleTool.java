package lab6.tools;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;

import cse115.graphics.DrawingCanvas;

public class TriangleTool implements ITool {
	private DrawingCanvas _dc;
	private Cursor _cursor;
	public TriangleTool(DrawingCanvas dc){
		_dc=dc;
		_cursor=_dc.makeCustomCursor("src/IMAGES/Circle.png");	
	}
	
	

	@Override
	public void apply(Point p) {
		// TODO Auto-generated method stub
		System.out.println("Triangle tool clicked");
		cse115.graphics.Triangle t=new cse115.graphics.Triangle();
		t.setColor(java.awt.Color.GREEN);
		t.setDimension(new Dimension(100,100));
		t.setLocation(p);
		_dc.add(t);

	}

	@Override
	public Cursor getCursor() {
		// TODO Auto-generated method stub
		
		return _cursor;
	}
}