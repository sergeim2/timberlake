package lab6.tools;

import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Point;
import cse115.graphics.DrawingCanvas;

public class EraserTool implements ITool {
	private DrawingCanvas _dc;
	private Cursor _cursor;
	
	public EraserTool(DrawingCanvas dc)
	{
		_dc=dc;
		_cursor = _dc.makeCustomCursor("src/IMAGES/Eraser.png");
		
	}
	public void apply(Point p){
		cse115.graphics.Graphic g=_dc.getGraphic(p);
		_dc.remove(g);
	}
	@Override
	public Cursor getCursor() {
		return _cursor;
	}
	
	
}
