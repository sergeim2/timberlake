package lab6.tools;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;

import cse115.graphics.DrawingCanvas;

public class CircleTool implements ITool {
	private DrawingCanvas _dc;
	private Cursor _cursor;
	public CircleTool(DrawingCanvas dc){
		_dc=dc;
		_cursor=_dc.makeCustomCursor("src/IMAGES/Circle.png");	
	}
	
	

	@Override
	public void apply(Point p) {
		// TODO Auto-generated method stub
		System.out.println("circle tool clicked");
		cse115.graphics.Ellipse c=new cse115.graphics.Ellipse();
		c.setColor(java.awt.Color.BLUE);
		c.setDimension(new Dimension(100,100));
		c.setLocation(p);
		_dc.add(c);

	}

	@Override
	public Cursor getCursor() {
		// TODO Auto-generated method stub
		
		return _cursor;
	}
}