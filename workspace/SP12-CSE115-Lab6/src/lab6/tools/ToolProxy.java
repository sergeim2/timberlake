package lab6.tools;

import java.awt.Cursor;
import java.awt.Point;

import cse115.graphics.DrawingCanvas;

public class ToolProxy implements ITool  {
private ITool _it;
private DrawingCanvas _dc;
	public ToolProxy(ITool it, DrawingCanvas dc)
{
	
	_dc=dc;
	setTool(it);
}
	public void setTool(ITool tool)
	{
		_it=tool;
		_dc.setCursor(tool.getCursor());
	}
	@Override
	public void apply(Point p) {
		_it.apply(p);
		
	}

	@Override
	public Cursor getCursor() {
		return _it.getCursor();
	}
}
