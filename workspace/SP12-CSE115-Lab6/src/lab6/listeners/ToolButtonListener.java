package lab6.listeners;

import java.awt.event.ActionEvent;

import lab6.tools.ITool;
import lab6.tools.ToolProxy;

public class ToolButtonListener implements java.awt.event.ActionListener {
	private ITool _it; 
	private ToolProxy _tp;
	public ToolButtonListener(ITool it, ToolProxy tp){
		_it=it;
		_tp=tp;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_tp.setTool(_it);
	}
	
}
