package lab6;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import lab6.listeners.CanvasListener;
import lab6.listeners.ToolButtonListener;
import lab6.tools.CircleTool;
import lab6.tools.EraserTool;
import lab6.tools.NullTool;
import lab6.tools.SquareTool;
import lab6.tools.ToolProxy;
import lab6.tools.TriangleTool;

import cse115.graphics.DrawingCanvas;

public class Lab6 {
	
	private JFrame _frame;
	private JPanel _tools;
	private DrawingCanvas _canvas;
	private ToolProxy _toolProxy;
	
	public Lab6() {

		// CREATE AND ORGANIZE THE WINDOW
		_frame = new JFrame("FA11-CSE115-Lab6-SOLUTION");
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		_frame.getContentPane().setLayout(new FlowLayout());

		// A PANEL FOR THE TOOL BUTTONS
		_tools = new JPanel();
		_tools.setBackground(java.awt.Color.LIGHT_GRAY);
		_tools.setLayout(new BoxLayout(_tools,BoxLayout.Y_AXIS));
		_frame.getContentPane().add(_tools);
		
		// A CANVAS FOR DRAWING
		_canvas = new DrawingCanvas();
		_canvas.setPreferredSize(new java.awt.Dimension(600,400));
		_canvas.setBackground(java.awt.Color.WHITE);
		_frame.getContentPane().add(_canvas);

		// ---- DO NOT EDIT ABOVE THIS LINE ----------
		// ---- BEGIN YOUR CODE HERE -----------------
		
		
		SquareTool st= new SquareTool(_canvas);
		//CanvasListener cl = new CanvasListener(st);
		//_canvas.addMouseListener(cl);
		//_canvas.setCursor(st.getCursor());
		
		CircleTool ct= new CircleTool(_canvas);
		//CanvasListener cl1 = new CanvasListener(ct);
		//_canvas.addMouseListener(cl1);
		//_canvas.setCursor(ct.getCursor());
		
		TriangleTool tt= new TriangleTool(_canvas);
		//CanvasListener cl2 = new CanvasListener(tt);
		//_canvas.addMouseListener(cl2);
		//_canvas.setCursor(tt.getCursor());
		
		NullTool nt= new NullTool();
		//CanvasListener cl3 = new CanvasListener(nt);
		//_canvas.addMouseListener(cl3);
		_canvas.setCursor(nt.getCursor());
		
		EraserTool et= new EraserTool(_canvas);
		//CanvasListener cl4 = new CanvasListener(et);
		//_canvas.addMouseListener(cl4);
		//_canvas.setCursor(st.getCursor());
		
		ToolProxy tp=new ToolProxy(nt, _canvas);
		CanvasListener cl5 = new CanvasListener(tp);
		_canvas.addMouseListener(cl5);
		//_canvas.setCursor(tp.getCursor());

		JButton bet = new JButton("Eraser",new ImageIcon("src/IMAGES/Eraser.png"));
		ToolButtonListener tblet=new ToolButtonListener(et, tp);
		JButton bnt = new JButton("NULL");
		ToolButtonListener tblnt=new ToolButtonListener(nt, tp);
		JButton bst = new JButton("Square",new ImageIcon("src/IMAGES/Square.png"));
		ToolButtonListener tblst=new ToolButtonListener(st,tp);
		JButton bct = new JButton("Circle",new ImageIcon("src/IMAGES/Circle.png"));
		ToolButtonListener tblct=new ToolButtonListener(ct,tp);
		JButton btt = new JButton("Triangle",new ImageIcon("src/IMAGES/Triangle.png"));
		ToolButtonListener tbltt=new ToolButtonListener(tt,tp);
		bet.addActionListener(tblet);
		bnt.addActionListener(tblnt);
		bst.addActionListener(tblst);
		bct.addActionListener(tblct);
		btt.addActionListener(tbltt);
		_tools.add(bet);
		_tools.add(bnt);
		_tools.add(bst);
		_tools.add(bct);
		_tools.add(btt);
		
		/*CircleTool et= new CircleTool(_canvas);
		CanvasListener cl2 = new CanvasListener(et);
		_canvas.addMouseListener(cl2);
		_canvas.setCursor(et.getCursor());
		
		TriangleTool tt= new TriangleTool(_canvas);
		CanvasListener cl3 = new CanvasListener(tt);
		_canvas.addMouseListener(cl3);
		_canvas.setCursor(tt.getCursor());
		*/

		// ---- END YOUR CODE HERE -------------------
		// ---- DO NOT EDIT BELOW THIS LINE ----------

		// STANDARD FRAME STUFF
		_frame.pack();
		_frame.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Lab6();
	}

}
