package user_interface.event_handlers;

import game_model.Board;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;


public class ButtonEventHandler implements ActionListener {

	private JButton _button;
	private Board _board;
	private int _r;
	private int _c;
	private JButton _l1;
	private JButton _l2;
	public ButtonEventHandler(JButton button, Board board, int col, int row,JButton l1,JButton l2) {
		_button = button;
		_board = board;
		_r=row;
		_c=col;
		_l1=l1;
		_l2=l2;
		//_board.put(col, row);
	}
	
	
	
	@Override public void actionPerformed(ActionEvent e) {
		_board.setListener(new UpdateListener(_button,_board,_l1,_l2));
		_board.put(_r,_c);
		
		
	}

}
