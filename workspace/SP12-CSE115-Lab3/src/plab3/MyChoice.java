package plab3;

public class MyChoice {
	private example1.Terrarium _terr;
	public MyChoice() {
	_terr = new example1.Terrarium();
	}
//ADDS ANT METHOD-----------
	public void AddsAnt(){
		example1.Ant locAnt;
	locAnt = new example1.Ant();	
		_terr.add(locAnt);
		locAnt.start();
	}
//---------------------------
	
//ADDS BUTTERFLY METHOD-------
	public void AddsButterfly(){
		example1.Butterfly locButterfly;
	locButterfly = new example1.Butterfly();	
		_terr.add(locButterfly);
		locButterfly.start();
	}
//---------------------------
	
//ADDS CATERPILLAR METHOD
	public void AddsCaterpillar(){
		example1.Caterpillar locCaterpillar;
	locCaterpillar = new example1.Caterpillar();	
		_terr.add(locCaterpillar);
		locCaterpillar.start();
	}
//----------------------------
}

