package plab3;

public class SecondClass {
	example1.Terrarium _t1;
	example1.Terrarium _t2;
	
	public SecondClass(){
		_t1 = new example1.Terrarium();
		_t2 = new example1.Terrarium();
	}
	
	//ADDS ANT METHOD-----------
	public void AddsAnt(){
		example1.Terrarium temp;
		example1.Ant locAnt;
	locAnt = new example1.Ant();	
		_t1.add(locAnt);
		locAnt.start();
		temp=_t2;
		_t2=_t1;
		_t1=temp;
	}
	
//---------------------------
	
//ADDS BUTTERFLY METHOD-------
	public void AddsButterfly(){
		example1.Terrarium temp;
		example1.Butterfly locButterfly;
	locButterfly = new example1.Butterfly();
	
		_t1.add(locButterfly);
		locButterfly.start();
		temp=_t2;
		_t2=_t1;
		_t1=temp;
	}
//---------------------------
	
//ADDS CATERPILLAR METHOD
	public void AddsCaterpillar(){
		example1.Terrarium temp;
		example1.Caterpillar locCaterpillar;
	locCaterpillar = new example1.Caterpillar();	
		_t1.add(locCaterpillar);
		locCaterpillar.start();
		temp=_t2;
		_t2=_t1;
		_t1=temp;
	}
//----------------------------

}
