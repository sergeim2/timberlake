/*	Sergei Mellow 
	50030322
	project 1
*/

//myhttpd [−d] [−h] [−l file] [−p port] [−r dir] [−t time] [−n threadnum] [−s sched]

//starting skeleton

static char svnid[] = "$Id: soc.c 6 2009-07-03 03:18:54Z kensmith $";

#define	BUF_LEN	8192

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<ctype.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netdb.h>
#include	<netinet/in.h>
#include	<inttypes.h>
#include        <pthread.h>
#include        <semaphore.h>
#include	<unistd.h>
#include	<time.h>
#include	<sys/stat.h>
#include	<arpa/inet.h>
#include	<dirent.h>
#include	<time.h>
#include	<errno.h>

 
char *progname;
char buf[BUF_LEN];

void usage();
int setup_client();
int setup_server();

int s, sock, ch, server, done, bytes, aflg;
int soctype = SOCK_STREAM;
char *host = NULL;
char *port = NULL;
extern char *optarg;
extern int optind;

int
setup_client() {

	struct hostent *hp, *gethostbyname();
	struct sockaddr_in serv;
	struct servent *se;

/*
 * Look up name of remote machine, getting its address.
 */
	if ((hp = gethostbyname(host)) == NULL) {
		fprintf(stderr, "%s: %s unknown host\n", progname, host);
		exit(1);
	}
/*
 * Set up the information needed for the socket to be bound to a socket on
 * a remote host.  Needs address family to use, the address of the remote
 * host (obtained above), and the port on the remote host to connect to.
 */
	serv.sin_family = AF_INET;
	memcpy(&serv.sin_addr, hp->h_addr, hp->h_length);
	if (isdigit(*port))
		serv.sin_port = htons(atoi(port));
	else {
		if ((se = getservbyname(port, (char *)NULL)) < (struct servent *) 0) {
			perror(port);
			exit(1);
		}
		serv.sin_port = se->s_port;
	}
/*
 * Try to connect the sockets...
 */
	if (connect(s, (struct sockaddr *) &serv, sizeof(serv)) < 0) {
		perror("connect");
		exit(1);
	} else
		fprintf(stderr, "Connected...\n");
	return(s);
}

/*
 * setup_server() - set up socket for mode of soc running as a server.
 */

int
setup_server() {
	struct sockaddr_in serv, remote;
	struct servent *se;
	int newsock, len;

	len = sizeof(remote);
	memset((void *)&serv, 0, sizeof(serv));
	serv.sin_family = AF_INET;
	if (port == NULL)
		serv.sin_port = htons(0);
	else if (isdigit(*port))
		serv.sin_port = htons(atoi(port));
	else {
		if ((se = getservbyname(port, (char *)NULL)) < (struct servent *) 0) {
			perror(port);
			exit(1);
		}
		serv.sin_port = se->s_port;
	}
	if (bind(s, (struct sockaddr *)&serv, sizeof(serv)) < 0) {
		perror("bind");
		exit(1);
	}
	if (getsockname(s, (struct sockaddr *) &remote, &len) < 0) {
		perror("getsockname");
		exit(1);
	}
	fprintf(stderr, "Port number is %d\n", ntohs(remote.sin_port));
	listen(s, 1);
	newsock = s;
	if (soctype == SOCK_STREAM) {
		fprintf(stderr, "Entering accept() waiting for connection.\n");
		newsock = accept(s, (struct sockaddr *) &remote, &len);
	}
	return(newsock);
}

/*
 * usage - print usage string and exit
 */

void
usage()
{
	fprintf(stderr, "usage: %s -h host -p port\n", progname);
	fprintf(stderr, "usage: %s -s [-p port]\n", progname);
	exit(1);
}

pthread_mutex_t mutx;
int ch;
int main(int argc,char *argv[])
{
//given code
	fd_set ready;
	struct sockaddr_in msgfrom;
	int msgsize;
	union {
		uint32_t addr;
		char bytes[4];
	} fromaddr;
//end of given code
	//default flags
	server=1;
	int d=0; //The web server would run as a daemon process in the background. 
	int h=0; //will not print summary of what program does!
	int l=0; //By default, myhttpd does not do any logging
	int p=0; //myhttpd will listen on port 8080
	int r=0; //means using defualt dictionary = /home/csdue/sergeime/cse421/project1 
	int t=0; //The default should be 60 seconds
	int n=0; //4 execution threads default
	int s=0; //FCFS=0 SJF=1
	//end of default flags
	//default variables
	char* defaultDir = strdup("/home/csdue/sergeime/cse421/project1/");
	char* defaultFile = strdup("file");
	//char defaultDir[50] ="/home/csdue/sergeime/cse421/project1";
	int defaultPort= 8080;
	int defaultTimer = 60;
	int defaultThreads = 4;
	//TIME TO PROCESS ALL INPUT FLAGS ASSUMING NO ERRORS ARE MAD
	int cargs=argc;	//cargs= current arguements left to process
	int flagCNT =argc;
	while ((ch = getopt(argc, argv, "dhlprtns")) != -1)
	{
	//debugging stuff
	//flagCNT=flagCNT-2;
	//printf("argc==%d",argc);
	//printf("\nflagCNT==%d",flagCNT);
		switch(ch) 
		{
			
			case 'd':
				flagCNT=flagCNT-1;
				l=1;
				d=1;
				flagCNT+1;
				printf("\nNot daemonizing, only accepting one connection at a time and enabling logging to stdout.(l=1,d=1)");		
				break;
			case 'h':
				flagCNT=flagCNT-1;
				h=1;
				flagCNT+1;
				printf("\n[−d] [−h] [−l file] [−p port] [−r dir][−t time][−n threadnum][−s sched]");
				break;
			case 'l':
				flagCNT=flagCNT-2;
				//printf("****flagCNT=%d********",flagCNT);
				l=1;
				printf("\nStarting to log server activities(l=1)\n");
				strcpy(defaultFile, argv[argc-flagCNT]);
				printf("\n%s%s\n",defaultDir,defaultFile);
				break;
			case 'p':
				flagCNT=flagCNT-2;
				defaultPort = atoi(argv[argc-flagCNT]);
				p=1;
				port=argv[argc-flagCNT];	
				//port = defaultPort;
				//Print port arg
				printf("\nwill use port specified by user(p=1)\n\tport=%d",defaultPort);
				break;
			case 'r':
				flagCNT=flagCNT-2;
				r=1;
				strcpy(defaultDir, argv[argc-flagCNT]);
				//defaultDir=atoi(argv[flagCNT]).c_str();
				printf("\nusing directory as specified by user(r=1)\n\tdir=");
				printf("%s%s%s\n",defaultDir,"/",defaultFile);
				break;
			case 't':
				flagCNT=flagCNT-2;
				//set timer
				t=1;
				defaultTimer=atoi(argv[argc-flagCNT]);
				printf("\ntimer set by user(t=1) \n\ttime=%d",defaultTimer);
				break;
			case 'n':
				flagCNT=flagCNT-2;
				//set threads
				n=1;
				defaultThreads=atoi(argv[argc-flagCNT]);
				printf("\nnumber of threads set by user(n=1) \n\tthreads=%d",defaultThreads);
				break;
			case 's':
				flagCNT=flagCNT-1;
				//make sure to use SFJ
				s=1;
				printf("\nUsing SJF instead of FCFS(s=1)\n");
				break;
			case '?':
			default:
			printf("myhttpd [−d] [−h] [−l file] [−p port] [−r dir] [−t time] [−n threadnum] [−s sched]");
			return (0);
		}//end of switch for setting flags
	}//end of while loop for arguements / flags
	//start of given code
	/*
 * Create socket on local host.
 */
	if ((s = socket(AF_INET, soctype, 0)) < 0) {
		perror("socket");
		exit(1);
	}
	if (!server)
		sock = setup_client();
	else
		sock = setup_server();
/*
 * Set up select(2) on both socket and terminal, anything that comes
 * in on socket goes to terminal, anything that gets typed on terminal
 * goes out socket...
 */
	while (!done) {
		FD_ZERO(&ready);
		FD_SET(sock, &ready);
		FD_SET(fileno(stdin), &ready);
		if (select((sock + 1), &ready, 0, 0, 0) < 0) {
			perror("select");
			exit(1);
		}
		if (FD_ISSET(fileno(stdin), &ready)) {
			if ((bytes = read(fileno(stdin), buf, BUF_LEN)) <= 0)
				done++;
			send(sock, buf, bytes, 0);
		}
		msgsize = sizeof(msgfrom);
		if (FD_ISSET(sock, &ready)) {
			if ((bytes = recvfrom(sock, buf, BUF_LEN, 0, (struct sockaddr *)&msgfrom, &msgsize)) <= 0) {
				done++;
			} else if (aflg) {
				fromaddr.addr = ntohl(msgfrom.sin_addr.s_addr);
				fprintf(stderr, "%d.%d.%d.%d: ", 0xff & (unsigned int)fromaddr.bytes[0],
			    	0xff & (unsigned int)fromaddr.bytes[1],
			    	0xff & (unsigned int)fromaddr.bytes[2],
			    	0xff & (unsigned int)fromaddr.bytes[3]);
			}
			write(fileno(stdout), buf, bytes);
		}
	}
	return(0);
}


//tried to use this but failed....
//http://stackoverflow.com/questions/8465006/how-to-concatenate-2-strings-in-c
char* concat(char *s1, char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    //in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}
