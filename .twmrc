# This is a default .twmrc file.
# It defines all the buttons and menu contents for the window manager.
# Feel free to experiment and change things.


# These are the colors of various things if you're on a monochrome system.
Monochrome
{
    BorderColor "black"
    BorderTileForeground "black"
    BorderTileBackground "white"
    TitleForeground "black"
    TitleBackground "white"
    MenuTitleForeground "white"
    MenuTitleBackground "black"
}

# These are the colors of various things if you're on a color system.
Color
{
    BorderColor "navy"
    BorderTileForeground "blue"
    BorderTileBackground "yellow"
    TitleForeground "white"
    TitleBackground "navy"
    MenuTitleForeground "white"
    MenuTitleBackground "navy"
    IconManagerForeground "white"
    IconManagerBackground "navy"
    IconManagerHighlight "green"
}

# Fonts for various things:
TitleFont	"9x15"
MenuFont	"9x15"
IconFont	"9x15"
ResizeFont	"9x15"
IconManagerFont	"9x15"

# Various global options  (see twm man page for more details)
InterpolateMenuColors
DecorateTransients
NoGrabServer
RestartPreviousState
BorderWidth	1
IconBorderWidth	1
MoveDelta 5

# If you want the cursor to move into a window when you deiconify it,
# uncomment the following line:
#WarpCursor

# If you want actual icons, rather than the icon manager, comment out
# the following lines:
IconifyByUnMapping
ShowIconManager
IconManagerGeometry		"=120x5-29+50"	1
SortIconManager


IconManagerDontShow {
	"xload" 
        "xclock"
        "dclock"
	"xbiff"
	"xeyes"
	"xmeter"
}


NoTitle {
    "xclock"
    "dclock"
    "TWM Icon Manager"
    "xload"
    "xbiff"
    "xeyes"
    "xmeter"
}

NoHighlight {
  "dclock"
  "xclock"
  "xload"
  "xeyes"
  "xmeter"
}

# If you want certain windows, or types of windows, to raise as soon as you
# move into them, add the window names to the following list, and uncomment
# this block of lines:
#AutoRaise
#{
#  "xterm"
#}


DefaultFunction f.menu "default-menu"

#Button = KEYS : CONTEXT : FUNCTION
#----------------------------------
Button1 =	: root		: f.menu "button1"
Button2 =	: root		: f.menu "button2"
Button3 =	: root		: f.menu "button3"
Button2 = m	: root		: f.menu "TwmWindows"

Button1 = c	: root		: f.menu "QuickProgs"

Button1 =	: t|f|i		: f.function "raise-lower-move"
Button1 = m	: t|f|w|i	: f.function "raise-lower-move"
Button2 = m	: t|f|w|i	: f.resize
Button3 = m	: t|f|w|i	: f.raise
Button3 =	: icon|iconmgr		: f.deiconify

"Up"    =	: iconmgr	: f.upiconmgr
"Down"  =	: iconmgr	: f.downiconmgr
"Left"  =	: iconmgr	: f.lefticonmgr
"Right" =	: iconmgr	: f.righticonmgr
"Left"  = m	: iconmgr	: f.previconmgr
"Right" = m	: iconmgr	: f.nexticonmgr

RightTitleButton "star" = f.delete

Function "raise-lower-move"
{
	f.move
	f.raiselower
}

Function "de-raise-n-focus"
{
    f.deiconify
    f.raise
    f.focus
}

Function "raise-n-focus"
{
    f.raise
    f.focus
}

menu "button1"
{
"Xterms"	f.title
"local window" ("blue":"white")	!"xterm -geometry 80x45+70+115 -ut&"
"ubunix"       !"xterm -geometry 80x24+50+155 -ut -n ubunix -T ubunix -e rlogin ubunix.acsu.buffalo.edu &"
"ssh"	      !"xprompt -re -p 'Host to ssh to' | xargs -i xterm -geometry 80x24+330+365 -title {} -ut -e ssh {} &"
"rlogin"	("blue":"white")      !"xprompt -re -p 'Host to rlogin to' | xargs -i xterm -geometry 80x24+330+365 -title {} -ut -e rlogin {} &"

"QuickProgs" ("white":"navy")	f.title
"xclipboard"	("blue":"white") 	!"xclipboard -geometry 250x200+0-0&"
"Netscape"	!"netscape &"
"Netscape (colors)"	!"netscape -install &"
"GhostView"	!"ghostview &"
"GV"		!"gv &"
"PageView"	!"pageview &"
"Xdvi"		!"xdvi &"
"xman"		!"xman &"
"xcalendar"	!"xcalendar &"
"audiocontrol"	!"audiocontrol &"
"xclock"	!"xclock &"
"dclock"	!"dclock &"
"xcalc"		!"xcalc -geometry +750+200 &"
"xemacs"	!"xemacs &"
"emacs"	 ("blue":"white")	!"emacs &"
}

menu "button2"
{
"Menus"					f.title
"WindowOps"	("white":"navy")	f.menu "winops"
"Movement"				f.menu "movement"
"Miscellany"				f.menu "misc"
"Logins"				f.menu "button1"
"FastOps"				f.menu "button3"
"Zoom"		("white":"navy")	f.menu "zoom"
}

menu "button3"
{
"FastOps"				f.title
"raise"		("black":"green")	f.raise
"resize"				f.resize
"move"					f.move
"restart twm"				f.restart
"refresh"				f.refresh
"iconify"				f.iconify
"delete window"				f.delete
"kill window"	("black":"green")	f.destroy
"Exit twm (log out)"	("white":"red")	f.quit
}

menu "movement"
{
"Movement"				f.title
"forcemove"				f.forcemove
"lower"					f.lower
"raise"					f.raise
"raiselower"				f.raiselower
}

menu "winops"
{
"Window Ops"				f.title
"autoraise"				f.autoraise
"identify"				f.identify
"refresh"				f.refresh
"winrefresh"				f.winrefresh
"unfocus"				f.unfocus
"deiconify"				f.deiconify
"focus"					f.focus
}

menu "misc"
{
"Misc"					f.title
"source twmrc" ("white":"blue")		f.twmrc
"restart"				f.restart
"version"				f.version
"showiconmgr"				f.showiconmgr
"hideiconmgr"				f.hideiconmgr
"sorticonmgr"				f.sorticonmgr
"cut exit"				^"exit"
"Exit twm"	("white":"blue")	f.quit
}

menu "zoom"
{
"zoom"					f.zoom
"fullzoom"				f.fullzoom
"leftzoom"				f.leftzoom
"rightzoom"				f.rightzoom
"topzoom"				f.topzoom
"bottomzoom"				f.bottomzoom
}


menu "default-menu"
{
"Default Menu"	("white":"firebrick4")	f.title
"Refresh"		f.refresh
"Refresh Window"	f.winrefresh
"twm Version"		f.version
"Focus on Root"		f.unfocus
"Source .twmrc"		f.twmrc
"Cut File"		f.cutfile
"(De)Iconify"		f.iconify
"DeIconify"		f.deiconify
"Move Window"		f.move
"ForceMove Window"	f.forcemove
"Resize Window"		f.resize
"Raise Window"		f.raise
"Lower Window"		f.lower
"Focus on Window"	f.focus
"Raise-n-Focus"		f.function "raise-n-focus"
"Destroy Window"	f.destroy
"Zoom Window"		f.zoom
"FullZoom Window"	f.fullzoom
"Kill twm"	("white":"firebrick4")	f.quit
}

Function "reset"
{
        f.unfocus
        f.twmrc
        f.refresh
        f.version
}
