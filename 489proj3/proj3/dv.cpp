#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include<iostream>
#include<fstream>

using namespace std;

// get sockaddr, IPv4 or IPv6: Beej's code
void *get_in_addr(struct sockaddr *sa)
{
if (sa->sa_family == AF_INET) {
return &(((struct sockaddr_in*)sa)->sin_addr);
}
return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
//============================================================
int main (int argc, char* argv[])
{
	
	string convertInt(int number); //func prototype to convert int's to string
	string argvT = argv[1];	//argv[1] aka "-t"
	string argvI=argv[3];	//argv[3] aka "-i"
	string MYIP;
	string MYPORT;
	int updateInterval=atoi(argv[4]);	//update interval
struct Server2NeighorCost	//this wil be an array for topofile struct 
	{
	int serverID;
	int neightborID;
	int cost;
	};
struct topofile			//this will be the topology file
	{
	int numOfServs;
	int numOfNeighs;
	string ID1;
	string ID2;
	string ID3;
	string ID4;
	string ID5;
	Server2NeighorCost a[4];	//maximum of 4 neights because only 5 servers	
	};
struct message  //refer to page 4 of PDF
	{
	string NOUF;
	string SP;
	string SIP;
	string SIPAN;
	string SPN;
	string SIDN;
	string COSTN;
	};	
struct pList //structure for keeping id/port listening on/ip address 
	{
	int id;
	int portListen;
	string ip;
	};
	int INPUT=0;
	//void printlist(pList a[]);
	//pList bsfill(pList a[]);

	//string convertInt(int number); //func prototype to convert int's to string
	cout <<argc<<argvT<<updateInterval<<endl;
	if(argc!=5) //ERROR HANDLING
	{
		cout<<"\nERROR! invalid command:\nTo start:./proj3 -t <topology file> -i <routing-interval-update>\n";
		return 0;
	}
	if(((argvT.compare("-t"))==0)&&(argvI.compare("-i")==0)) //corrent synatax for topology file input
	{
		
		topofile TOPOLOGY;	//for loop was bugging out so I had to excess code it...
		TOPOLOGY.numOfServs=0;
		TOPOLOGY.numOfNeighs=0;
		TOPOLOGY.ID1="";
		TOPOLOGY.ID2="";
		TOPOLOGY.ID3="";
		TOPOLOGY.ID4="";
		TOPOLOGY.ID5="";
		TOPOLOGY.a[3].serverID=0;	//this initiates everything to 0
		TOPOLOGY.a[3].neightborID=0;
		TOPOLOGY.a[3].cost=0;
		TOPOLOGY.a[2].serverID=0;
                TOPOLOGY.a[2].neightborID=0;
                TOPOLOGY.a[2].cost=0;
		TOPOLOGY.a[1].serverID=0;
                TOPOLOGY.a[1].neightborID=0;
		TOPOLOGY.a[1].cost=0;
		TOPOLOGY.a[0].serverID=0;
		TOPOLOGY.a[0].neightborID=0;
		TOPOLOGY.a[0].cost=0;
		//need to open topology file and fill up my topology struct!
		cout<<"\nFilename:"<<argv[2]<<"\nUpdate Interval:"<<updateInterval;
		//try to open argv[2] aka file name
		ifstream myReadFile;
		myReadFile.open(argv[2]);
		char output[100];
		if (myReadFile.is_open()) {
			int p=0;
			int k=0;
			while (!myReadFile.eof()) 
			{	
				
				myReadFile >> output;
				if(p==0)// means num of servrs
					TOPOLOGY.numOfServs=atoi(output);
				if(p==1)//means num of neighbors
					TOPOLOGY.numOfNeighs=atoi(output);
				while((p>1)&&(TOPOLOGY.numOfServs*3>=p-1))
				{
				//cout<<output<<endl;
				if(p==2)//p=2 id1
					TOPOLOGY.ID1=output; 
				if(p==3)//p=3 id1's ip
					TOPOLOGY.ID1=TOPOLOGY.ID1.append(" ").append(output); 
				if(p==4)//p=4 id1's port
					TOPOLOGY.ID1=TOPOLOGY.ID1.append(" ").append(output);	
    			        if(p==5)//p=5 id2
					TOPOLOGY.ID2=output;
                                if(p==6)//p=6 id2's ip
					 TOPOLOGY.ID2=TOPOLOGY.ID2.append(" ").append(output);
                                if(p==7)//p=7 id2's port
					 TOPOLOGY.ID2=TOPOLOGY.ID2.append(" ").append(output);
				if(p==8)//p=8 id3
				 	 TOPOLOGY.ID3=output;
                                if(p==9)//p=9 id3's ip
					 TOPOLOGY.ID3=TOPOLOGY.ID3.append(" ").append(output);
                                if(p==10)//p=10 id3's portp=
					 TOPOLOGY.ID3=TOPOLOGY.ID3.append(" ").append(output);
				if(p==11)//p=11 id4
					 TOPOLOGY.ID4=output;
                                if(p==12)//p=12 id4's ip
					 TOPOLOGY.ID4=TOPOLOGY.ID4.append(" ").append(output);
                                if(p==13)//p=13 id4's portp=3
					 TOPOLOGY.ID4=TOPOLOGY.ID4.append(" ").append(output);
				if(p==14)//p=14 id5
					 TOPOLOGY.ID5=output;
                                if(p==15)//p=15 id5's ip
					 TOPOLOGY.ID5=TOPOLOGY.ID5.append(" ").append(output);
                                if(p==16)//p=16 id5's portp=3
					 TOPOLOGY.ID5=TOPOLOGY.ID5.append(" ").append(output);
				p++;
				myReadFile >> output;
				}
				while((p>=TOPOLOGY.numOfServs*3)&&!myReadFile.eof())
                                {
					if(k==0)
						TOPOLOGY.a[0].serverID=atoi(output);
					if(k==1)
				        	TOPOLOGY.a[0].neightborID=atoi(output);
					if(k==2)
        					TOPOLOGY.a[0].cost=atoi(output);
					if(k==3)
                                                TOPOLOGY.a[1].serverID=atoi(output);
                                        if(k==4)
                                                TOPOLOGY.a[1].neightborID=atoi(output);
                                        if(k==5)
                                                TOPOLOGY.a[1].cost=atoi(output);
					if(k==6)
                                                TOPOLOGY.a[2].serverID=atoi(output);
                                        if(k==7)
                                                TOPOLOGY.a[2].neightborID=atoi(output);
                                        if(k==8)
                                                TOPOLOGY.a[2].cost=atoi(output);
					if(k==9)
                                                TOPOLOGY.a[3].serverID=atoi(output);
                                        if(k==10)
                                                TOPOLOGY.a[3].neightborID=atoi(output);
                                        if(k==11)
                                                TOPOLOGY.a[3].cost=atoi(output);
					k++;
					myReadFile >> output;
					if(myReadFile.eof())
					{
						break;
					}					
					p++;
					
				}
				p++;//hard coding
			}
		}
		myReadFile.close();
		try{	
		cout<<"\nnum of servers:"<<TOPOLOGY.numOfServs;	
		cout<<"\nnum of neighbors:"<<TOPOLOGY.numOfNeighs;
		cout<<"\nID1:"<<TOPOLOGY.ID1;
		cout<<"\nID2:"<<TOPOLOGY.ID2;
		cout<<"\nID3:"<<TOPOLOGY.ID3;
		cout<<"\nID4:"<<TOPOLOGY.ID4;
		cout<<"\nID5:"<<TOPOLOGY.ID5;
		cout<<"\nhost:"<<TOPOLOGY.a[0].serverID<<" neighbor:"<<TOPOLOGY.a[0].neightborID<<" cost:"<<TOPOLOGY.a[0].cost;
		cout<<"\nhost:"<<TOPOLOGY.a[1].serverID<<" neighbor:"<<TOPOLOGY.a[1].neightborID<<" cost:"<<TOPOLOGY.a[1].cost;
		cout<<"\nhost:"<<TOPOLOGY.a[2].serverID<<" neighbor:"<<TOPOLOGY.a[2].neightborID<<" cost:"<<TOPOLOGY.a[2].cost;
		cout<<"\nhost:"<<TOPOLOGY.a[3].serverID<<" neighbor:"<<TOPOLOGY.a[3].neightborID<<" cost:"<<TOPOLOGY.a[3].cost<<endl;
		}
		catch(char * str)
		{
			cout << "some bs happened but its ok " << str << '\n';
		}
//--------------------------------------------------------------------------------------
                 //use port 53 and 8.8.8.8 and google dns to get my own ip
                 int sock_fdDNS, pton_retDNS;
                 struct sockaddr_in local_addrDNS, server_addrDNS;
                 char local_address[13];
                 uint16_t remote_port = 53;
                 //set up socket to connect to google dns
                 if((sock_fdDNS=socket(AF_INET, SOCK_STREAM,0))<0)
                 {
                         cout<<"\nERROR MAKING SOCKET FOR OWN PORT";
                         exit(1);
                 }
                 //fill structure to connect
                 memset((void *) &local_addrDNS, 0, sizeof(local_addrDNS));
                 memset((void *) &server_addrDNS, 0, sizeof(server_addrDNS));
                 server_addrDNS.sin_family = AF_INET;
                 server_addrDNS.sin_port = htons(remote_port);
                 pton_retDNS =inet_pton(AF_INET, "8.8.8.8", &server_addrDNS.sin_addr);
                 if(pton_retDNS<=0)
                 {
                        cout<<"\nPTON INET ERROR";
                        exit(1);
                 }
                 //connecting to google dns
                 if(connect(sock_fdDNS, (struct sockaddr *)&server_addrDNS,sizeof(server_addrDNS))<0)
                 {
                         cout<<"\nerror connectinging";
                         exit(1);
                 }
                 //getting my own ip
                 int length = sizeof(local_addrDNS);
                 getsockname(sock_fdDNS, (struct sockaddr *) &local_addrDNS, (socklen_t*) &length);
                 strcpy(local_address,inet_ntoa(local_addrDNS.sin_addr));
                 //DONE GETTING IP ADRESS FOR SELF  
//---------------------------------------------------------------------------------------------------
		//I could of either used code above or topology file to get my IP
		if(TOPOLOGY.a[0].serverID==1)
		{
	 		MYIP=TOPOLOGY.ID1.substr(2,TOPOLOGY.ID1.length()-7);
			MYPORT=TOPOLOGY.ID1.substr(TOPOLOGY.ID1.length()-4,TOPOLOGY.ID1.length());
		}
		if(TOPOLOGY.a[0].serverID==2)
		{	
			MYIP=TOPOLOGY.ID2.substr(2,TOPOLOGY.ID2.length()-7);
			MYPORT=TOPOLOGY.ID1.substr(TOPOLOGY.ID2.length()-4,TOPOLOGY.ID2.length());
		}
		if(TOPOLOGY.a[0].serverID==3)
		{
			MYIP=TOPOLOGY.ID3.substr(2,TOPOLOGY.ID3.length()-7);
			MYPORT=TOPOLOGY.ID1.substr(TOPOLOGY.ID3.length()-4,TOPOLOGY.ID3.length());
		}
		if(TOPOLOGY.a[0].serverID==4)
		{	
			MYIP=TOPOLOGY.ID4.substr(2,TOPOLOGY.ID4.length()-7);
			MYPORT=TOPOLOGY.ID1.substr(TOPOLOGY.ID4.length()-4,TOPOLOGY.ID4.length());
		}
		if(TOPOLOGY.a[0].serverID==5)
		{	
			MYIP=TOPOLOGY.ID5.substr(2,TOPOLOGY.ID5.length()-7);
			MYPORT=TOPOLOGY.ID1.substr(TOPOLOGY.ID5.length()-4,TOPOLOGY.ID5.length());
		}
		cout<<"Currnent servers IP:"<<MYIP<<endl;
		cout<<"currnet servers PORt:"<<MYPORT<<endl;		
		//setting 
		//return 0;
		//end of filling topology struct
		cout<<"\nHit ENTER to continue";
		int connectionID;
		string cmd;
		cout<<"\n"<<"SERVER STARTING...\nType Command:\n";
		string timb="128.205.36.8";
		struct pList PL[5];
		PL[0].id=0;	//inserting server itself into its own list
		PL[0].portListen=atoi(argv[2]);
		PL[0].ip=timb;
                for(int k=1;k<5;k++)//making the rest 0 values so it will print.
                        {
                               PL[k].id=k;
                               PL[k].portListen=0;
                               PL[k].ip="";
                        }
		
		int ServerListenPort = atoi(MYPORT.c_str());
		string SLPS = convertInt(ServerListenPort);
		char * PORT = new char [SLPS.length()+1];
		std::strcpy (PORT, SLPS.c_str());
		//beejs from here on out
		fd_set master; // master file descriptor list
		fd_set read_fds; // temp file descriptor list for select()

		int fdmax; // maximum file descriptor number
		int listener; // listening socket descriptor
		int newfd; // newly accept()ed socket descriptor
		struct sockaddr_storage remoteaddr; // client address
		socklen_t addrlen;

		char buf[256]; // buffer for client data
		int nbytes;
		char remoteIP[INET6_ADDRSTRLEN];
		int yes=1; // for setsockopt() SO_REUSEADDR, below
		int i, j, rv;
		struct addrinfo hints, *ai, *p;
		FD_ZERO(&master); // clear the master and temp sets
		FD_ZERO(&read_fds);

		// get us a socket and bind it
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_flags = AI_PASSIVE;

		if ((rv = getaddrinfo(NULL,  PORT, &hints, &ai)) != 0) {
			fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
			exit(1);
		}
		for(p = ai; p != NULL; p = p->ai_next) {
			listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
			if (listener < 0) {
				continue;
			}
			// lose the pesky "address already in use" error message
			setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
			if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
				close(listener);
				continue;
			}

			break;
		}
		// if we got here, it means we didn't get bound //didn't get bound?????????????????????
		if (p == NULL) {

			fprintf(stderr, "selectserver: failed to bind\n");
			exit(2);
		}
		freeaddrinfo(ai); // all done with this
		// listen
		if (listen(listener, 5) == -1) {
			perror("listen");
			close(listener);
			exit(3);
		}

		// add the listener to the master set
		FD_SET(listener, &master);
		FD_SET(INPUT, &master);
		// keep track of the biggest file descriptor
		fdmax = listener; // so far, it's this one
		// main loop
for(;;) {	
		FD_SET(INPUT,&master);
		read_fds = master; // copy it //FURTHUEST IT GOT WAS IN HERE
		
		if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
		perror("select");
		exit(4);
		}
	// run through the existing connections looking for data to read
	for(i = 0; i <= fdmax; i++) {
		if (FD_ISSET(INPUT, &read_fds)) 
		{ 
			// we got one!!
			fgets(buf,80,stdin);
                        cout<<"user ~/ >";
                        cmd="";
                        cin >> cmd;
                        if(cmd=="crash")
                        {
				cout<<"\n Crashing now:"<<endl;
				while(true)
				{
					 cout<<"user ~/ >";
	        	                 cmd="";
        	                	 cin >> cmd;
					 if(cmd=="display")
					{
                                 	cout<<"\nnum of servers:";
                                 	cout<<"\nnum of neighbors:";
                                 	cout<<"\nID1:";
                                 	cout<<"\nID2:";
                                 	cout<<"\nID3:";
                                 	cout<<"\nID4:";
                                 	cout<<"\nID5:";
                                 	cout<<"\nhost:"<<" neighbor:"<<" cost:";
                                	cout<<"\nhost:"<<" neighbor:"<<" cost:";
                                	 cout<<"\nhost:"<<" neighbor:"<<" cost:";
                                 cout<<"\nhost:"<<" neighbor:"<<" cost:"<<endl;

                         }

				}
				//need to terminate all connections
                                exit(0);
                        }
                        else if(cmd=="step")    
                        {
                                cout<<"\n sending routing update to other rotuers...:"<<endl;
                                break;
                        } 
			else if(cmd=="packets")    
                        {
                                cout<<"\npackets:0"<<endl;
                                break;
                        }
                        else if(cmd=="display")
                        {	//write code to display topo file
                                cout<<"\nnum of servers:"<<TOPOLOGY.numOfServs;
		                cout<<"\nnum of neighbors:"<<TOPOLOGY.numOfNeighs;
		                cout<<"\nID1:"<<TOPOLOGY.ID1;
      			        cout<<"\nID2:"<<TOPOLOGY.ID2;
        		        cout<<"\nID3:"<<TOPOLOGY.ID3;
		                cout<<"\nID4:"<<TOPOLOGY.ID4;
		                cout<<"\nID5:"<<TOPOLOGY.ID5;
		                cout<<"\nhost:"<<TOPOLOGY.a[0].serverID<<" neighbor:"<<TOPOLOGY.a[0].neightborID<<" cost:"<<TOPOLOGY.a[0].cost;
		                cout<<"\nhost:"<<TOPOLOGY.a[1].serverID<<" neighbor:"<<TOPOLOGY.a[1].neightborID<<" cost:"<<TOPOLOGY.a[1].cost;
               			cout<<"\nhost:"<<TOPOLOGY.a[2].serverID<<" neighbor:"<<TOPOLOGY.a[2].neightborID<<" cost:"<<TOPOLOGY.a[2].cost;
            			cout<<"\nhost:"<<TOPOLOGY.a[3].serverID<<" neighbor:"<<TOPOLOGY.a[3].neightborID<<" cost:"<<TOPOLOGY.a[3].cost<<endl;

				break;
                        }
                        else if(cmd=="disable")
                        {	//write code for substrings
				cout<<"\nEnter ID to disable";
				cin>>connectionID;
				cout<<"\n Disabling ID:"<<connectionID<<"..."<<endl;
				
				//need some more code to send out to all clients
                                //clear ID and update all clients of client leave
                                break;
                        }
                        else if(cmd=="update")
                        {//write update code
				cout<<"\n Updating link cost between two servers...\n";
                                break;
                        }
                        else if(cmd=="HELP")
                        {
				cout<<"\n I'm the one that needs help not you.\n";
                                break;
                        }
                        else if(cmd=="CREATOR")
                        {
                                cout<<"\nWelcome to Sergei Mellow's(sergeime 50030322) futile attempt at Project 3\n";
                                break;
                        }
                        else
                        {
                                cout<<"\nError did not recognize command: type HELP for commands\n";
                                break;
                        }
			FD_CLR(INPUT,&read_fds);	
		}

		
			//big chuck taken here
		else if (FD_ISSET(i, &read_fds)) {
			if (i == listener) {
				// handle new connections
				addrlen = sizeof remoteaddr;
				newfd = accept(listener,(struct sockaddr *)&remoteaddr,&addrlen);
				if (newfd == -1) {
					perror("accept");
				} else {
				FD_SET(newfd, &master); // add to master set
					if (newfd > fdmax) { // keep track of the max
						fdmax = newfd;
					}
				
				printf("selectserver: new connection from %s on "
				"socket %d\n",
				inet_ntop(remoteaddr.ss_family,
				get_in_addr((struct sockaddr*)&remoteaddr),remoteIP, INET6_ADDRSTRLEN),newfd);
				for(int t=0;t<5;t++){
					if(PL[t].portListen==0)
					{
						PL[t].id=t; 
                                		PL[t].portListen = newfd;
                                		PL[t].ip=remoteIP;
                                		break;
				        }
					if(t==5)
						return 0;                        
                       		 }
				}
			} 
			else 
			{
				// handle data from a client
				if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
				// got error or connection closed by client
					if (nbytes == 0) {
					// connection closed
					printf("selectserver: socket %d hung up\n", i);
					} else {
					perror("recv");
					}
					close(i); // bye!
					FD_CLR(i, &master); // remove from master set
				} else {
				 //we got some data from a client
					for(j = 0; j <= fdmax; j++) {
						// send to everyone!
						if (FD_ISSET(j, &master)) {
							//read nybates into list and then send nybvates to others...
							// except the listener and ourselves
							//UPDATE LIST
							//SEND LIST
							if (j != listener && j != i) {
								if (send(j, buf, nbytes, 0) == -1) {
								perror("send");
								}
							}
						}
					}
		
				}
			} // END handle data from client
		} // END got new incoming connection
	} // END looping through file descriptors
} // END for(;;)--and you thought it would never end!
	
}//END OF SERVER CODE
	else
	{
		cout<<"\ninvalid command:\nTo start:./proj1 s <port no> or ./proj1 c <port no> e.g.:./proj1 s 12345\n";
		return 0;
	}//CATCHES ERRORS WHEN CREATING CLIENT / SERVER

return 0;
}
string convertInt(int number) //http://www.cplusplus.com/forum/beginner/7777/
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
