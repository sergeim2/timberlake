#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

using namespace std;

// get sockaddr, IPv4 or IPv6: Beej's code
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
int main (int argc, char* argv[])
{
	string convertInt(int number); //func prototype to convert int's to string

	struct pList //structure for keeping id/port listening on/ip address 
	{
		int id;
		int portListen;
		string ip;
	};
	int INPUT=0;
	//void printlist(pList a[]);
	//pList bsfill(pList a[]);

	//string convertInt(int number); //func prototype to convert int's to string
	if(argc!=3) //ERROR HANDLING
	{
		cout<<"\nERROR! invalid command:\nTo start:./proj1 s <port no> or ./proj1 c <port no> e.g.:./proj1 s 12345\n";
		return 0;
	}
	if(*argv[1]=='s') //CODING UP THE SERVER
	{
		cout<<"\nHit ENTER to continue and use HELP command for a list of all COMMANDS";
		int connectionID;
		string cmd;
		cout<<"\n"<<argv[1]<<"erver:TIMBERLAKE SERVER STARTING...\nPreparing to listen for incoming connections for list requests\n";
		string timb="128.205.36.8";
		struct pList PL[5];
		PL[0].id=0;	//inserting server itself into its own list
		PL[0].portListen=atoi(argv[2]);
		PL[0].ip=timb;
		for(int k=1;k<5;k++)//making the rest 0 values so it will print.
		{
			PL[k].id=k;
			PL[k].portListen=0;
			PL[k].ip="";
		}
		int ServerListenPort = atoi(argv[2]);
		string SLPS = convertInt(ServerListenPort);
		char * PORT = new char [SLPS.length()+1];
		std::strcpy (PORT, SLPS.c_str());
		//beejs from here on out
		fd_set master; // master file descriptor list
		fd_set read_fds; // temp file descriptor list for select()

		int fdmax; // maximum file descriptor number
		int listener; // listening socket descriptor
		int newfd; // newly accept()ed socket descriptor
		struct sockaddr_storage remoteaddr; // client address
		socklen_t addrlen;

		char buf[256]; // buffer for client data
		int nbytes;
		char remoteIP[INET6_ADDRSTRLEN];
		int yes=1; // for setsockopt() SO_REUSEADDR, below
		int i, j, rv;
		struct addrinfo hints, *ai, *p;
		FD_ZERO(&master); // clear the master and temp sets
		FD_ZERO(&read_fds);

		// get us a socket and bind it
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_flags = AI_PASSIVE;

		if ((rv = getaddrinfo(NULL,  PORT, &hints, &ai)) != 0) {
			fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
			exit(1);
		}
		for(p = ai; p != NULL; p = p->ai_next) {
			listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
			if (listener < 0) {
				continue;
			}
			// lose the pesky "address already in use" error message
			setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
			if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
				close(listener);
				continue;
			}

			break;
		}
		// if we got here, it means we didn't get bound //didn't get bound?????????????????????
		if (p == NULL) {

			fprintf(stderr, "selectserver: failed to bind\n");
			exit(2);
		}
		freeaddrinfo(ai); // all done with this
		// listen
		if (listen(listener, 5) == -1) {
			perror("listen");
			close(listener);
			exit(3);
		}

		// add the listener to the master set
		FD_SET(listener, &master);
		FD_SET(INPUT, &master);
		// keep track of the biggest file descriptor
		fdmax = listener; // so far, it's this one
		// main loop
		for(;;) {	
			FD_SET(INPUT,&master);
			read_fds = master; // copy it //FURTHUEST IT GOT WAS IN HERE

			if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
				perror("select");
				exit(4);
			}
			// run through the existing connections looking for data to read
			for(i = 0; i <= fdmax; i++) {
				if (FD_ISSET(INPUT, &read_fds)) 
				{ 
					// we got one!!
					fgets(buf,80,stdin);
					cout<<"user ~/ >";
					cmd="";
					cin >> cmd;
					if(cmd=="EXIT")
					{
						//need to terminate all connections
						exit(0);
					}
					else if(cmd=="MYIP")    //clients ip
					{
						cout<<"\nMY IP Address:"<<timb<<endl;
						break;
					}
					else if(cmd=="LIST")
					{
						for(int k=0;k<5;k++)
						{
							cout<<"ID:"<<PL[k].id<<"   ";
							cout<<"IP:"<<PL[k].ip<<"   ";
							cout<<"Listen Port:"<<PL[k].portListen<<endl;
						}
						break;
					}
					else if(cmd=="TERMINATE")
					{
						cout<<"\nEnter ID to terminate:";
						cin>>connectionID;
						for(int t=1;t<5;t++){
							if(PL[t].id==connectionID)
							{
								PL[t].id=0;
								PL[t].portListen = 0;
								PL[t].ip="0";
								break;
							}
						}
						//need some more code to send out to all clients
						//clear ID and update all clients of client leave
						break;
					}
					else if(cmd=="MYPORT")
					{
						cout<<"my port:"<<SLPS<<endl;
						break;
					}
					else if(cmd=="HELP")
					{
						cout<<"\nList of commands:\nCREATOR - Display name of creator\nMYPORT - Displays your own client port";
						cout<<"\nMYIP - Shows Local ip\nTERMINATE-disconnect specific client";
						cout<<"\nEXIT - Close program(de register from server)\nLIST - Lists of all clients\n";
						break;
					}
					else if(cmd=="CREATOR")
					{
						cout<<"\nWelcome to Sergei Mellow's(sergeime 50030322) server/client program\n";
						break;
					}
					else
					{
						cout<<"\nError did not recognize command: type HELP for commands\n";
						break;
					}
					FD_CLR(INPUT,&read_fds);	
				}


				//big chuck taken here
				else if (FD_ISSET(i, &read_fds)) {
					if (i == listener) {
						// handle new connections
						addrlen = sizeof remoteaddr;
						newfd = accept(listener,(struct sockaddr *)&remoteaddr,&addrlen);
						if (newfd == -1) {
							perror("accept");
						} else {
							FD_SET(newfd, &master); // add to master set
							if (newfd > fdmax) { // keep track of the max
								fdmax = newfd;
							}

							printf("selectserver: new connection from %s on "
									"socket %d\n",
									inet_ntop(remoteaddr.ss_family,
											get_in_addr((struct sockaddr*)&remoteaddr),remoteIP, INET6_ADDRSTRLEN),newfd);
							for(int t=0;t<5;t++){
								if(PL[t].portListen==0)
								{
									PL[t].id=t; 
									PL[t].portListen = newfd;
									PL[t].ip=remoteIP;
									break;
								}
								if(t==5)
									return 0;                        
							}
						}
					} 
					else 
					{
						// handle data from a client
						if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
							// got error or connection closed by client
							if (nbytes == 0) {
								// connection closed
								printf("selectserver: socket %d hung up\n", i);
							} else {
								perror("recv");
							}
							close(i); // bye!
							FD_CLR(i, &master); // remove from master set
						} else {
							//we got some data from a client
							for(j = 0; j <= fdmax; j++) {
								// send to everyone!
								if (FD_ISSET(j, &master)) {
									//read nybates into list and then send nybvates to others...
									// except the listener and ourselves
									//UPDATE LIST
									//SEND LIST
									if (j != listener && j != i) {
										if (send(j, buf, nbytes, 0) == -1) {
											perror("send");
										}
									}
								}
							}

						}
					} // END handle data from client
				} // END got new incoming connection
			} // END looping through file descriptors
		} // END for(;;)--and you thought it would never end!

	}//END OF SERVER CODE
	else if(*argv[1]=='c') //client code
	{	
		cout<<"\nHit any ENTER to continue";
		char regIP[13]; //used for register and connect
		struct pList PL[5];
		for(int k=0;k<5;k++)
		{
			PL[k].id=0;
			PL[k].portListen=0;
			PL[k].ip="";
		}
		int regPort; //used for register and connect
		int connectionID; //send connection id
		string sendm; //send message
		int myPortInt=atoi(argv[2]);  //int my port

		int tcpPort=myPortInt;          //string my port
		string MPIS = convertInt(myPortInt);
		//--------------------------------------------------------------------------------------

		//use port 53 and 8.8.8.8 and google dns to get my own ip
		int sock_fdDNS, pton_retDNS;
		struct sockaddr_in local_addrDNS, server_addrDNS;
		char local_address[13];
		uint16_t remote_port = 53;
		//set up socket to connect to google dns
		if((sock_fdDNS=socket(AF_INET, SOCK_STREAM,0))<0)
		{
			cout<<"\nERROR MAKING SOCKET FOR OWN PORT";
			exit(1);
		}
		//fill structure to connect
		memset((void *) &local_addrDNS, 0, sizeof(local_addrDNS));
		memset((void *) &server_addrDNS, 0, sizeof(server_addrDNS));
		server_addrDNS.sin_family = AF_INET;
		server_addrDNS.sin_port = htons(remote_port);
		pton_retDNS =inet_pton(AF_INET, "8.8.8.8", &server_addrDNS.sin_addr);
		if(pton_retDNS<=0)
		{
			cout<<"\nPTON INET ERROR";
			exit(1);
		}
		//connecting to google dns
		if(connect(sock_fdDNS, (struct sockaddr *)&server_addrDNS,sizeof(server_addrDNS))<0)
		{
			cout<<"\nerror connectinging";
			exit(1);
		}
		//getting my own ip
		int length = sizeof(local_addrDNS);
		getsockname(sock_fdDNS, (struct sockaddr *) &local_addrDNS, (socklen_t*) &length);
		strcpy(local_address,inet_ntoa(local_addrDNS.sin_addr));
		//DONE GETTING IP ADRESS FOR SELF 
		//---------------------------------------------------------------------------------------------------
		//setting up listening FOR CLIENT BIG STEPP!!
		//---------------------------------------------------------------------------
		//for select
		int ServerListenPort = atoi(argv[2]);
		string SLPS = convertInt(ServerListenPort);
		char * PORT = new char [SLPS.length()+1];
		std::strcpy (PORT, SLPS.c_str());

		//beejs from here on out
		//const char * PORT = SLPS.c_str();
		fd_set master; // master file descriptor list
		fd_set read_fds; // temp file descriptor list for select()
		int fdmax; // maximum file descriptor number
		int listener; // listening socket descriptor
		int newfd; // newly accept()ed socket descriptor
		struct sockaddr_storage remoteaddr; // client address
		socklen_t addrlen;

		char buf[256]; // buffer for client data
		int nbytes;
		char remoteIP[INET6_ADDRSTRLEN];
		int yes=1; // for setsockopt() SO_REUSEADDR, below
		int i, j, rv;
		struct addrinfo hints, *ai, *p;
		FD_ZERO(&master); // clear the master and temp sets
		FD_ZERO(&read_fds);

		// get us a socket and bind it
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_flags = AI_PASSIVE;

		if ((rv = getaddrinfo(NULL,  PORT, &hints, &ai)) != 0) {
			fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
			exit(1);
		}
		for(p = ai; p != NULL; p = p->ai_next) {
			listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
			if (listener < 0) {
				continue;
			}
			// lose the pesky "address already in use" error message
			setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
			if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
				close(listener);
				continue;
			}

			break;
		}
		// if we got here, it means we didn't get bound //didn't get bound?????????????????????
		if (p == NULL) {

			fprintf(stderr, "selectserver: failed to bind\n");
			exit(2);
		}
		freeaddrinfo(ai); // all done with this
		// listen
		if (listen(listener, 5) == -1) {
			perror("listen");
			close(listener);
			exit(3);
		}

		// add the listener to the master set
		FD_SET(listener, &master);
		FD_SET(INPUT, &master);
		// keep track of the biggest file descriptor
		fdmax = listener; // so far, it's this one
		cout<<"\n "<<"Welcome to Sergei Mellow's(sergeime 50030322) client type 'HELP' for list of all commands\n";
		// main loop
		for(;;) {
			FD_SET(INPUT,&master);
			read_fds = master; // copy it //FURTHUEST IT GOT WAS IN HERE

			if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
				perror("select");
				exit(4);
			}
			// run through the existing connections looking for data to read
			for(i = 0; i <= fdmax; i++) {
				if (FD_ISSET(INPUT, &read_fds))
				{
					// we got one!!
					fgets(buf,80,stdin);

					string cmd="";	


					cout<<"user ~/ >";
					cmd="";
					cin >> cmd;
					if(cmd=="EXIT")
					{
						exit(0);
					}
					else if(cmd=="MYIP")	//clients ip
					{
						cout<<"\nMY IP Address:"<<local_address<<endl;
						continue;
					}
					else if(cmd=="REGISTER")
					{	
						cout<<"\nPlease Enter server IP:";
						cin>>regIP;
						cout<<"\nEnter Port on which timberlake server is running on:";
						cin>>regPort;
						cout<<"\nRegistering to IP:"<<regIP<<" on port:"<<regPort;
						int sock_fdT, pton_retT;
						struct sockaddr_in local_addrT, server_addrT;
						char local_addressT[13];
						uint16_t remote_portT = regPort;
						//set up socket to connect to timberlake
						if((sock_fdT=socket(AF_INET, SOCK_STREAM,0))<0)
						{
							cout<<"\nERROR MAKING SOCKET FOR OWN PORT";
							exit(1);
						}
						//fill structure to connect
						memset((void *) &local_addrT, 0, sizeof(local_addrT));
						memset((void *) &server_addrT, 0, sizeof(server_addrT));
						server_addrT.sin_family = AF_INET;
						server_addrT.sin_port = htons(remote_portT);
						//regit
						pton_retT =inet_pton(AF_INET, "128.205.36.8", &server_addrT.sin_addr);
						if(pton_retT<=0)
						{
							cout<<"\nPTON INET ERROR";
							exit(1);
						}
						//connecting to timberlake server
						if(connect(sock_fdT, (struct sockaddr *)&server_addrT,sizeof(server_addrT))<0)
						{
							cout<<"\nerror connectinging";
							exit(1);
						}
						cout<<"\nConnected, trying to register\n";
						//sending my ip and port listening on to update list on timberlake
						int len, bytes_sent,len2,bytes_sent2;
						char* msg1=local_address;
						len = strlen(msg1);
						bytes_sent = send(sock_fdT, msg1, len, 0);

						char * msg2 = new char [MPIS.length()+1];
						len2 = strlen(msg2);
						bytes_sent2 = send(sock_fdT, msg2, len2, 0);

						//getsockname(sock_fdT, (struct sockaddr *) &local_addrT, (socklen_t*) &length);
						//strcpy(local_address,inet_ntoa(local_addrT.sin_addr));


						continue;
					}
					else if(cmd=="CONNECT")
					{	
						cout<<"\nPlease enter peer's IP:";
						cin>>regIP;
						cout<<"\nPlease enter port on which peer is listening on";
						cin>>regPort;
						cout<<"\nconnecting to IP:"<<regIP<<" on port:"<<regPort;



						continue;
					}
					else if(cmd=="LIST")
					{
						for(int k=0;k<5;k++)
						{
							cout<<"ID:"<<PL[k].id<<"   ";
							cout<<"IP:"<<PL[k].ip<<"   ";
							cout<<"Listen Port:"<<PL[k].portListen<<endl;
						}
						continue;
					}
					else if(cmd=="SEND")
					{	
						cout<<"\nPlease enter connection ID:";
						cin>>connectionID;
						cout<<"\nMessage:";
						cin>>sendm;
						continue;
					}
					else if(cmd=="TERMINATE")
					{
						cout<<"\nEnter ID to terminate:";
						cin>>connectionID;
						//check if i am connected to ID or not.... thne terminate if i am
						continue;
					}
					else if(cmd=="MYPORT")
					{
						cout<<"my port:"<<MPIS<<endl;
						continue;
					}
					else if(cmd=="HELP")
					{
						cout<<"\nList of commands:\nCREATOR - Display name of creator\nMYPORT - Displays your own client port";
						cout<<"\nMYIP - Shows Local ip\nREGISTER - Register your client with the server and update all clients server lists ";
						cout<<"\nCONNECT - Connects to  a max of 3 peers and server\nLIST - Lits server and all peers";
						cout<<"\nSEND - Sends text message to another peer\nTERMINATE - Disconnect from peer";
						cout<<"\nEXIT - Close program(de register from server)\n";

						continue;
					}
					else if(cmd=="CREATOR")
					{
						cout<<"\nWelcome to Sergei Mellow's(sergeime 50030322) server/client program\n";
						continue;
					}


					else
					{
						cout<<"\nError did not recognize command: type HELP for commands\n";
						continue;
					}
				}
				//big chuck taken here
				else if (FD_ISSET(i, &read_fds)) {
					if (i == listener) {
						// handle new connections
						addrlen = sizeof remoteaddr;
						newfd = accept(listener,(struct sockaddr *)&remoteaddr,&addrlen);
						if (newfd == -1) {
							perror("accept");
						} else {
							FD_SET(newfd, &master); // add to master set
							if (newfd > fdmax) { // keep track of the max
								fdmax = newfd;
							}

							printf("selectserver: new connection from %s on "
									"socket %d\n",
									inet_ntop(remoteaddr.ss_family,
											get_in_addr((struct sockaddr*)&remoteaddr),remoteIP, INET6_ADDRSTRLEN),newfd);
							for(int t=0;t<5;t++){
								if(PL[t].portListen==0)
								{
									PL[t].id=t;
									PL[t].portListen = newfd;
									PL[t].ip=remoteIP;
									break;
								}
								if(t==5)
									return 0;
							}
						}
					}
					else
					{
						// handle data from a client
						if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
							// got error or connection closed by client
							if (nbytes == 0) {
								// connection closed
								printf("selectserver: socket %d hung up\n", i);
							} else {
								perror("recv");
							}
							close(i); // bye!
							FD_CLR(i, &master); // remove from master set
						} else {
							//we got some data from a client
							for(j = 0; j <= fdmax; j++) {
								// send to everyone!
								if (FD_ISSET(j, &master)) {
									//read nybates into list and then send nybvates to others...
									// except the listener and ourselves
									//UPDATE LIST
									//SEND LIST
									if (j != listener && j != i) {
										if (send(j, buf, nbytes, 0) == -1) {
											perror("send");
										}
									}
								}
							}
						}
					} // END handle data from client
				} // END got new incoming connection

			}		
		}	}//END OF CLIENT
	else
	{
		cout<<"\ninvalid command:\nTo start:./proj1 s <port no> or ./proj1 c <port no> e.g.:./proj1 s 12345\n";
		return 0;
	}//CATCHES ERRORS WHEN CREATING CLIENT / SERVER

	return 0;
}
string convertInt(int number) //http://www.cplusplus.com/forum/beginner/7777/
{
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}
