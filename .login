# This is the default '.login' file.  This file is read only at login time.
# INITIAL LOGIN SCRIPT.

stty crterase crt erase '^H' kill '^U'

if ($TERM == 'su' || $TERM == 'network' || $TERM == 'unknown') then

	set myterm=""

	while ($myterm == "")
		echo ""
		echo -n "Enter desired terminal type (or 'help'): "
		set myterm=$<

		if ($myterm == "help" || $myterm == "'help'") then
			more << EOF

In order for UNIX to take advantage of the full screen features on your
terminal it must know what kind of terminal you have. If you are using
a terminal in one of the computer sites, chances are pretty good that
the type of your terminal is a 'vt100'. If you are dialing in from home
your terminal type depends on the software you used to connect. Look
in the manual under 'terminal emulation' to try to determine which kind
of terminal you have. If you still cannot determine the type of your
terminal and you feel adventurous you can just try one. The 'vt100' type
is quite common and a good place to start. You should be aware that
just choosing random types can lock up your terminal, though, and can
require you to start over. Or you can always play it safe with the
'unknown' type which will not assume anything about your terminal. Be
aware, however, that the unknown type does not allow you to run any
editor, such as vi or emacs, and possibly other software. Most other
tools should function properly. Good luck and if you continue to have
trouble please send mail to 'consult' for more help.
EOF
			set myterm=""
		endif
	end

	set term=$myterm
	unset myterm

endif

echo ""

limit coredumpsize 1
